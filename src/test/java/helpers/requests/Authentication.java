package helpers.requests;

import com.google.gson.JsonObject;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import helpers.dataBase.DataBaseManipulation;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class Authentication {

    public static RequestSpecification createHeader(String contentType){
        RequestSpecification request = RestAssured.given();
        return request.header("Content-Type", contentType);
    }

    public static RequestSpecification createRequest(JsonObject testUser, String contentType){
        RequestSpecification request = createHeader(contentType);
        return request.body(testUser);
    }

    public static Response registerUser(JsonObject testUser) {
        return createRequest(testUser, "application/json").post(Property.getProperty("registerShortUrl"));
    }

    public static Response validateUser(JsonObject testUser) {
        return createRequest(testUser, "application/json").post(Property.getProperty("confirmAccountShortUrl"));
    }

    public static Response logInUser(JsonObject testUser) {
        return createRequest(testUser, "application/json").post(Property.getProperty("authenticateShortUrl"));
    }


    public static String logInUserAndGetToken(JsonObject testUser) {
        return createRequest(testUser, "application/json").post(Property.getProperty("authenticateShortUrl")).jsonPath().getJsonObject("jwt");
    }

    public static Response sendResetPasswordEmail(String email) {
        JsonObject testUser = new JsonObject();
        testUser.addProperty("email", email);
        return createRequest(testUser, "application/json").post(Property.getProperty("forgotPasswordShortUrl"));
    }

    public static Response resetPassword(JsonObject passwordAndToken) {
        return createRequest(passwordAndToken, "application/json").post(Property.getProperty("resetPasswordShortUrl"));
    }

    public static String registerLoginReturnToken(JsonObject testUser){
        registerUser(testUser);
        testUser.addProperty("token", DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "confirmation_token"));
        validateUser(testUser);
        return createRequest(testUser, "application/json").post(Property.getProperty("authenticateShortUrl")).jsonPath().getJsonObject("jwt");
    }
}