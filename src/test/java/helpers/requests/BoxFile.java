package helpers.requests;

import helpers.utils.Property;
import helpers.pojo.file.DeleteResponsePojo;
import helpers.pojo.file.UploadResponsePojo;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class BoxFile {

    private static UploadResponsePojo[] testResponse;
    private static DeleteResponsePojo[] deleteResponse;

    public static Response uploadFiles(File[] files, String folderName, int projectID, String token, ArrayList<Boolean> check) {
        Response response;
        RequestSpecification request = uploadFilesReq(files, folderName, token, "multipart/form-data", "files");
        response = request.post(Property.getProperty("uploadFileShortUrl") + projectID);
        if ((response.statusCode() != 200) && (response.statusCode() != 207)) return response;
        else
        testResponse = response.getBody().as(UploadResponsePojo[].class);
        for (int i = 0; i < files.length; i++) {
            check.add(testResponse[i].getSuccessful());
        }
        return response;
    }

    public static File createRandomFile(String extension) throws IOException {
        return File.createTempFile("file", extension, new File(System.getProperty("user.dir") + "/filesForUpload"));
    }

    public static File[] createRandomFiles(int numberOfFiles) throws IOException {
        File[] fileArray = new File[numberOfFiles];
        for (int i = 0; i < fileArray.length; i++) {
            fileArray[i] = BoxFile.createRandomFile(".txt");
            fileArray[i].deleteOnExit();
        }
        return fileArray;
    }

    public static Response downloadFiles(int projectID, File[] fileName, String folderName, String token){
        Response response;
        RequestSpecification request = downloadFilesReq(fileName, folderName, token , "application/json");
        response = request.post(Property.getProperty("projectsShortUrl") + projectID + "/file");
        return  response;
    }

    public static Response deleteFiles(File[] files, String folderName, int projectID, String token, ArrayList<Boolean> check){
        Response response;
        RequestSpecification request = deleteFilesReq(files, folderName, token, "application/json");
        response = request.delete(Property.getProperty("projectsShortUrl") + projectID + "/files/");
        if ((response.statusCode() != 207) && (response.statusCode() != 200)) return response;
        deleteResponse = response.getBody().as(DeleteResponsePojo[].class);
        for (int i = 0; i < files.length; i++) {
            check.add(deleteResponse[i].getSuccessful());
        }
        return response;
    }

    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", contentType);
        request.header("Authorization", token);
        return request;
    }

    public static RequestSpecification uploadFilesReq(File[] file, String folderName, String token, String contentType, String controlName){
        RequestSpecification request = createHeader(token, contentType);
        for (File files : file) {
            request.multiPart("files", files);
        }
        request.multiPart("path", folderName);
        return request;
    }

    public static RequestSpecification deleteFilesReq(File[] fileName, String folderName, String token, String contentType) {
        RequestSpecification request = createHeader(token, "application/json");
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < fileName.length; i++) {
            if (folderName.equals("")) {
                sb.append("\"").append(fileName[i].getName()).append("\"");
            } else {
                sb.append("\"").append(folderName).append("/").append(fileName[i].getName()).append("\"");
            }
            if (fileName.length > i + 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        request.body(sb.toString());
        return request;
    }

    public static RequestSpecification downloadFilesReq(File[] fileName, String folderName, String token, String contentType) {
        RequestSpecification request = createHeader(token, "application/json");
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (int i = 0; i <= fileName.length - 1; i++) {
            sb.append("\"path\":\"");
            if (folderName.equals("")) {
                sb.append(fileName[i].getName()).append("\"");
            } else {
                sb.append(folderName).append("/").append(fileName[i].getName()).append("\"");
            }
            if (fileName.length - 1 > i) {
                sb.append(", ");
            }
        }
        sb.append("}");
        request.body(sb.toString());
        return request;
    }
}