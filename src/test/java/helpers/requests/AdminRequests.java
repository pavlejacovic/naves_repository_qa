package helpers.requests;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import helpers.pojo.user.ChangePasswordRequestPojo;
import helpers.pojo.user.ChangeUserInformationRequestPojo;
import helpers.utils.Property;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AdminRequests {

    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        if (contentType != null)
            request.header("Content-Type", contentType);
        if (token != null)
            request.header("Authorization", token);
        return request;
    }

    public static String getAdminToken(){
        JsonObject adminBody = new JsonObject();
        adminBody.addProperty("email", Property.getProperty("admin"));
        adminBody.addProperty("password", Property.getProperty("adminPassword"));
        RequestSpecification request = createHeader(null, "application/json");
        return Authentication.logInUserAndGetToken(adminBody);
    }

    public static Response getNumberOfClients(String token){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("adminUsersShortUrl"));
    }

    public static Response getNumberOfProjects(String token){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("adminProjectsShortUrl"));
    }

    public static Response getProjectsWithDetails(String token){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("adminProjectsShortUrl") + "/projects");
    }


    public static Response getProjectsWithPagination(String token, int size, int page){
        RequestSpecification request = createHeader(token, null);
        request.queryParam("size", size);
        request.queryParam("page", page);
        return request.get(Property.getProperty("adminProjectsShortUrl") + "/projects/page");
    }

    public static Response getNumberOfFunctions(String token){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("adminFunctionsShortUrl"));
    }

}
