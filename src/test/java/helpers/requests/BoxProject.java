package helpers.requests;
import com.google.gson.JsonObject;
import helpers.pojo.project.ShareProjectRequestPojo;
import helpers.utils.Property;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class BoxProject {

    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        if (contentType != null)
            request.header("Content-Type", contentType);
        request.header("Authorization", token);
        return request;
    }

    public static RequestSpecification createRequest(JsonObject projectBody, String token){
        RequestSpecification request = createHeader(token, "application/json");
        if (projectBody != null)
        request.body(projectBody);
        return request;
    }

    public static Response createProject(JsonObject projectBody, String token){
        return createRequest(projectBody, token).post(Property.getProperty("projectsShortUrl"));
    }

    public static Response renameProject(JsonObject projectBody, String token, int projectID){
        return createRequest(projectBody, token).put(Property.getProperty("projectsShortUrl") + projectID);
    }

    public static Response deleteProject(String token, int projectID){
        return createRequest(null, token).delete(Property.getProperty("projectsShortUrl") + projectID);
    }

    public static Response listProjects(String token){
        return createRequest(null, token).get(Property.getProperty("projectsShortUrl"));
    }
    public static Response listProjectsWithPagination(String token, int viewSize, int startPage, String listOrder){
        RequestSpecification request = createHeader(token, "application/json");
        request.queryParam("size", viewSize);
        request.queryParam("page", startPage);
        request.queryParam("sort", listOrder);
        return request.get(Property.getProperty("projectsShortUrl") + "page");
    }

    public static int makeRandomProjectAndGetId(String logInToken, String email){
        String randomProjectName = GenerateRandom.randomDataName();
        JsonObject createProjectBody = new JsonObject();
        createProjectBody.addProperty("projectName", randomProjectName);
        BoxProject.createProject(createProjectBody, logInToken);
        return DataBaseManipulation.getProjectID(email, randomProjectName);
    }

    public static Response shareProject(String token, int projectID, ShareProjectRequestPojo[] shareRequest){
        RequestSpecification request = createHeader(token, "application/json");
        return request.body(shareRequest).post(Property.getProperty("projectsShortUrl") + projectID + "/share");
    }

    public static Response listSharedUsers(String token, int projectID){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("projectsShortUrl") + projectID + "/share");
    }

}