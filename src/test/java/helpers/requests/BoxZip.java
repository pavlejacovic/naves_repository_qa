package helpers.requests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.File;

public class BoxZip {

    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", contentType);
        request.header("Authorization", token);
        return request;
    }

    public static RequestSpecification createRequest(File[] files, String folder, String token, String contentType){
        RequestSpecification request = createHeader(token, contentType);
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < files.length; i++){
            sb.append("{\"path\": \"");
            if (!folder.isEmpty()) sb.append(folder);
                    if (files.length != 0) sb.append("/");
            sb.append(files[i].getName()).append("\"}");
            if (files.length - 1> i) sb.append(", ");
        }
        sb.append("]");
        return request.body(sb.toString());
    }

    public static Response downloadFilesAsZip(File[] files, String folder, String token, int projectID){
        return createRequest(files, folder, token, "application/json").post("/box/projects/" + projectID + "/files/zip");
    }

    public static Response downloadProjectAsZip(String token, int projectID){
        RequestSpecification request = createHeader(token, "application/json");
        return request.body("[{\"path\": \"\"}]").post("/box/projects/" + projectID + "/files/zip");
    }

    public static Response downloadFolderAsZip(String folder, String token, int projectID){
        RequestSpecification request = createHeader(token, "application/json");
        return request.body("[{\"path\": \"" + folder + "\"}]").post("/box/projects/" + projectID + "/files/zip");
    }

}