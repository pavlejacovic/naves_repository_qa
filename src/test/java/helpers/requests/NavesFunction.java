package helpers.requests;

import helpers.utils.Property;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.File;

public class NavesFunction {

    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        if (contentType != null)
            request.header("Content-Type", contentType);
        request.header("Authorization", token);
        return request;
    }

    public static Response createNavesFunction(String token, String functionName, Boolean publicAccess, String trigger, File file, String language, Integer[] projectIds){
        RequestSpecification request = createHeader(token, "multipart/form-data");
        if (functionName != null)
            request.multiPart("functionName", functionName);
        if (publicAccess != null)
            request.multiPart("publicAccess", publicAccess);
        if (trigger != null)
            request.multiPart("trigger", trigger);
        if (file != null)
            request.multiPart("archiveFile", file);
        if (language != null)
            request.multiPart("language", language);
        if (projectIds != null)
            for (int projectID : projectIds)
                request.multiPart("projectIds", projectID);
        return request.post(Property.getProperty("navesFunctionShortUrl"));
    }

    public static Response updateFunctionProperties(String token, String functionName, Boolean publicAccess, String trigger, Integer[] projectIds, int functionID, String language){
        RequestSpecification request = createHeader(token, "multipart/form-data");
        if (functionName != null)
            request.multiPart("functionName", functionName);
        if (publicAccess != null)
            request.multiPart("publicAccess", publicAccess);
        if (trigger != null)
            request.multiPart("trigger", trigger);
        if (language != null)
            request.multiPart("language", language);
        if (projectIds != null)
            for (int projectID : projectIds)
                request.multiPart("projectIds", projectID);
        return request.put(Property.getProperty("navesFunctionShortUrl") + "functions/properties/" + functionID);
    }

    public static Response deleteNavesFunction(String token, int functionID){
        RequestSpecification request = createHeader(token, null);
        return request.delete(Property.getProperty("navesFunctionShortUrl")  + "functions/" + functionID);
    }

    public static Response updateNavesFunctionExecution(String token, File file, int functionID, String language){
        RequestSpecification request = createHeader(token, "multipart/form-data");
        request.multiPart("archiveFile", file);
        request.multiPart("language", language);
        return request.put(Property.getProperty("navesFunctionShortUrl") + "functions/execution/" + functionID);
    }

    public static Response listAllFunctions(String token){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("navesFunctionShortUrl") + "functions");
    }

    public static Response listAllFunctionsWithPagination(String token, int size, int page){
        RequestSpecification request = createHeader(token, null);
        request.queryParam("size", size);
        request.queryParam("page", page);
        return request.get(Property.getProperty("navesFunctionShortUrl") + "functions/page/");
    }

    public static Response executeFunction(String token, String parameter, String value, String method, String functionLink, String contentType){
        Response response = null;
        RequestSpecification request = createHeader(token, contentType);
        if (parameter != null)
        request.multiPart(parameter, value);
        if (method.equals("POST")){
            response = request.post(Property.getProperty("navesExecuteShortUrl") + functionLink);
        }
        else if (method.equals("GET")){
            response = request.get(Property.getProperty("navesExecuteShortUrl") + functionLink);
        }
        return response;
    }

    public static Response generateNewLink(String token, int functionID){
        RequestSpecification request = createHeader(token, null);
        return request.patch(Property.getProperty("navesFunctionLinkShortUrl") + functionID + "/link" );
    }

    public static Response getStatistics(String token, int functionID, int daysOfExecution){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("navesFunctionStatisticsLinkShortUrl") + functionID + "/" + daysOfExecution);
    }
}