package helpers.requests;

import helpers.pojo.file.RenameRequestPojo;
import helpers.pojo.folder.CreateFolderPojo;
import helpers.pojo.folder.ListAllResponsePojo;
import helpers.utils.Property;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class BoxFolder {



    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", contentType);
        request.header("Authorization", token);
        return request;
    }

    public static Response createFolder(String folderName, int projectID, String token){
        CreateFolderPojo createFolder = new CreateFolderPojo("/", folderName);
        RequestSpecification request = createHeader(token, "application/json");
        request.body(createFolder);
        return request.post(Property.getProperty("foldersShortUrl") + projectID);
    }

    public static Response delete(String fileOrFolder, int projectID, String token) {
        RequestSpecification request = createHeader(token, "application/json");
        request.body("[\"" + fileOrFolder + "\"]");
        return request.delete(Property.getProperty("projectsShortUrl") + projectID + "/files/");
    }

    public static Response delete(String[] filesOrFolders, int projectID, String token){
        RequestSpecification request = createHeader(token, "application/json");
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < filesOrFolders.length; i++){
            sb.append("\"").append(filesOrFolders[i]).append("\"");
            if (filesOrFolders.length > i+1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        request.body(sb.toString());
        return request.delete(Property.getProperty("projectsShortUrl") + projectID + "/files/");
    }

    public static Response rename(RenameRequestPojo folderBody, String token, int projectID){
        RequestSpecification request = createHeader(token, "application/json");
        request.body(folderBody);
        return request.put(Property.getProperty("projectsShortUrl") + projectID + "/files");
    }

    public static Response listAll(String path, int projectID, String token){
        Response response;
        RequestSpecification request = createHeader(token ,"application/json");
        request.body("{\"path\":" + "\"" + path + "\"}");
        response = request.post(Property.getProperty("projectsShortUrl") + projectID + "/folders");
        return response;
    }
}