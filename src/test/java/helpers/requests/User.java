package helpers.requests;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import helpers.pojo.user.ChangePasswordRequestPojo;
import helpers.pojo.user.ChangeUserInformationRequestPojo;
import helpers.utils.Property;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class User {

    public static RequestSpecification createHeader(String token, String contentType){
        RequestSpecification request = RestAssured.given();
        if (contentType != null)
            request.header("Content-Type", contentType);
        if (token != null)
            request.header("Authorization", token);
        return request;
    }

    public static Response getUserInformation(String token){
        RequestSpecification request = createHeader(token, null);
        return request.get(Property.getProperty("userProfile"));
    }

    public static Response changeUserInformation(String token, ChangeUserInformationRequestPojo userInformation){
        RequestSpecification request = createHeader(token, "application/json");
        Gson gson = new Gson();
        request.body(gson.toJson(userInformation));
        return request.put(Property.getProperty("userProfile"));
    }

    public static void resetUser(String token, JsonObject testUser){
        ChangeUserInformationRequestPojo requestPojo = new ChangeUserInformationRequestPojo();
        requestPojo.setName(testUser.get("name").getAsString());
        requestPojo.setSurname(testUser.get("surname").getAsString());
        requestPojo.setSubscribed(testUser.get("subscribed").getAsString());
        RequestSpecification request = createHeader(token, "application/json");
        Gson gson = new Gson();
        request.body(gson.toJson(requestPojo));
        request.put(Property.getProperty("userProfile"));
    }

    public static Response changePassword(String token, ChangePasswordRequestPojo changePasswordPojo){
        RequestSpecification request = createHeader(token, "application/json");
        Gson gson = new Gson();
        request.body(gson.toJson(changePasswordPojo));
        return request.put(Property.getProperty("changePassword"));
    }

    public static void resetPassword(String token, String oldPassword, JsonObject testUser){
        ChangePasswordRequestPojo requestPojo = new ChangePasswordRequestPojo();
        requestPojo.setOldPassword(oldPassword);
        requestPojo.setNewPassword(testUser.get("password").getAsString());
    }
}
