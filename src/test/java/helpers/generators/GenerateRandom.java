//Sasa Avramovic
package helpers.generators;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import java.util.Locale;
import java.util.Random;

public class GenerateRandom {

    public static FakeValuesService fakeValues = new FakeValuesService(new Locale("en-GB"), new RandomService());
    public static Faker faker = new Faker();


    public static String randomValidName(){
        return faker.name().firstName().replaceAll("[^A-Za-z]", "");
    }

    public static String randomValidSurname() {
        String surname = faker.name().lastName().replaceAll("[^A-Za-z]", "");;
        return surname;
    }

    public static char randomSpecialCharacter(){
        String chars = " !@#$%&";
        Random rnd = new Random();
        return chars.charAt(rnd.nextInt(chars.length()));
    }

    public static String randomDataName() {
        return fakeValues.regexify("[a-z1-9]{10}");
    }

    public static String createEmail(String name, String surname){
        return ("sasaavramovic82+" + name + "." + surname + "@gmail.com").toLowerCase(Locale.ROOT);
    }

    public static String randomValidPassword(){
        String capitalChars = "QWERTYUIOPASDFGHJKLZXCVBNM";
        String smallChars = "qwertyuiopasdfghjklzxcvbnm";
        String numerals = "1234567890";
        String specialChars = "!@#$%^&*()";
        StringBuilder sb = new StringBuilder();
        Random rnd = new Random();
        for (int i = 0; i < 2; i ++)sb.append(capitalChars.charAt(rnd.nextInt(capitalChars.length())));
        for (int i = 0; i < 2; i ++)sb.append(smallChars.charAt(rnd.nextInt(smallChars.length())));
        for (int i = 0; i < 2; i ++)sb.append(numerals.charAt(rnd.nextInt(numerals.length())));
        for (int i = 0; i < 2; i ++)sb.append(specialChars.charAt(rnd.nextInt(specialChars.length())));
        return sb.toString();
    }
}
