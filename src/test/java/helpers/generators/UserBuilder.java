//Sasa Avramovic

package helpers.generators;
import com.google.gson.JsonObject;
import helpers.Constants;
import helpers.utils.Property;


public class UserBuilder {

    public static JsonObject testUser() {
        JsonObject testUser = new JsonObject();
        String name = GenerateRandom.randomValidName();
        String lastName = GenerateRandom.randomValidSurname();
        testUser.addProperty("email", GenerateRandom.createEmail(name, lastName));
        testUser.addProperty("password", Constants.validPasswordEightChars);
        testUser.addProperty("provider", "LOCAL");
        testUser.addProperty("name", name);
        testUser.addProperty("surname", lastName);
        testUser.addProperty("subscribed", "true");
        return testUser;
    }
}