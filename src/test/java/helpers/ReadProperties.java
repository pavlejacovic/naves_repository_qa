package helpers;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.ConfigFactory;

@Config.Sources("common.properties")
public interface ReadProperties extends Config {



  String dbUser();
  String dbPass();
  String dbUrl();
  String admin();
  String adminPassword();
  String noGateway();

  @Key("baseUri")
  String baseUri();

  String frontUri();
  String authenticationUri();
  String mainPageUri();
  String forgotPassUri();
  String registerUri();
  String loginUri();
  String resetPasswordUri();
  String functionsUri();
  String userProfilePage();
  String foldersShortUrl();
  String adminProjectsShortUrl();
  String adminUsersShortUrl();
  String adminFunctionsShortUrl();
  String projectsShortUrl();
  String registerShortUrl();
  String confirmAccountShortUrl();
  String authenticateShortUrl();
  String forgotPasswordShortUrl();
  String resetPasswordShortUrl();
  String uploadFileShortUrl();
  String navesFunctionShortUrl();
  String navesExecuteShortUrl();
  String navesFunctionLinkShortUrl();
  String navesFunctionStatisticsLinkShortUrl();
  String userProfile();
  String changePassword();
}