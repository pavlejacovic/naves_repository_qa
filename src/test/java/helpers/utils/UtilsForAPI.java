package helpers.utils;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import helpers.dataBase.DataBaseManipulation;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Callable;

import static helpers.dataBase.DataBaseManipulation.getQueryResult;

public class UtilsForAPI {

    public static Path findPath(String email, int projectID, String folder) {
        return Paths.get(Constants.rootFolder, email.toLowerCase(Locale.ROOT), String.valueOf(projectID), folder);
    }

    public static Path findPath(String email, int projectID, String folder, String file) {
        return Paths.get(Constants.rootFolder, email.toLowerCase(Locale.ROOT), String.valueOf(projectID), folder, file);
    }

    public static String rootFolderSelector() {
        String rootFolder = null;
        if (Objects.equals(System.getProperty("os.name"), "Linux")) rootFolder =
                Paths.get(System.getProperty("user.dir") + "/../naves_repository_be/project service/fileSystem/").normalize().toString();
        if (Objects.equals(System.getProperty("os.name"), "Windows 10")) rootFolder = "C:\\fileSystem\\";
        return rootFolder;
    }

    public static String downloadFolderSelector() {
        String downloadFolder = null;
        if (Objects.equals(System.getProperty("os.name"), "Linux")) downloadFolder =
                Paths.get(System.getProperty("user.home") + "/Downloads/").normalize().toString();
        if (Objects.equals(System.getProperty("os.name"), "Windows 10")) downloadFolder =
                Paths.get(System.getProperty("user.home") + "\\Downloads\\").normalize().toString();
        return downloadFolder;
    }

    public static Callable<Boolean> containerStarted(DockerClient dockerClient, List<Container> allContainers, String email, String functionName) {
        return () -> {
            allContainers.clear();
            allContainers.addAll(dockerClient.listContainersCmd().exec());
            return allContainers.get(0).getImage().equals(functionName + "-" + DataBaseManipulation.getFromDatabase(email, "user_id")); // The condition that must be fulfilled
        };
    }

    public static Callable<Boolean> functionUploaded(DockerClient dockerClient, List<Container> allContainers, String email, String functionName) {
        return () -> {
                ResultSet result = getQueryResult("SELECT * FROM naves3.lambda_function WHERE function_name = \"" + functionName + "-" + DataBaseManipulation.getFromDatabase(email, "user_id") + "\"");
            return result.next();
        };
        }


}