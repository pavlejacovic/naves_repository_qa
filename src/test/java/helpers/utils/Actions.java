//Sasa Avramovic
package helpers.utils;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.ProjectsPage;

import static pageObjects.ProjectsPage.projects_toastError_message;

public class Actions {

    public static void sendTextToElement(WebDriver driver, WebDriverWait wait, By element, String text) {
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(element)));
        driver.findElement(element).clear();
        driver.findElement(element).sendKeys(text);
    }

    public static void checkCheckbox(WebDriver driver, WebDriverWait wait, By checkbox){
        if (!driver.findElement(checkbox).isSelected()){
            driver.findElement(checkbox).click();
        }
    }

    public static void uncheckCheckbox(WebDriver driver, WebDriverWait wait, By checkbox){
        if (driver.findElement(checkbox).isSelected()){
            driver.findElement(checkbox).click();
        }
    }

    public static boolean waitForElementToBeVisible(WebDriver driver, WebDriverWait wait, By element){
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(element)));
        return false;
    }

    public static boolean waitForElementToBeClickable(WebDriver driver, WebDriverWait wait, By element){
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(element)));
        return false;
    }

    public static boolean checkToastMessage(WebDriver driver, WebDriverWait wait, By toastMessage){
        Actions.waitForElementToBeVisible(driver, wait, toastMessage);
        boolean result = (driver.findElement(toastMessage).isDisplayed());
        driver.findElement(toastMessage).findElement(ProjectsPage.projects_toastClose_button).sendKeys(Keys.ENTER);
        return result;
    }
}