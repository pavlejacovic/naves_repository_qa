package helpers.utils;

import java.io.*;
import java.util.Properties;
public class Property {
    private static String property;

    public static String getProperty(String key) {

        try (InputStream input = new FileInputStream("src/test/java/common.properties")) {
            java.util.Properties prop = new java.util.Properties();
            prop.load(input);
            property = prop.getProperty(key);
        } catch (IOException e) {
            e.printStackTrace();
        } return property;
    }

}
