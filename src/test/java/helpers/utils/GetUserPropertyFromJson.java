//Sasa Avramovic
package helpers.utils;

import com.google.gson.JsonObject;

public class GetUserPropertyFromJson {

    public static String getEmail(JsonObject user) {
        return user.get("email").toString().replace("\"", "");
    }

    public static String getName(JsonObject user) {
        return user.get("name").toString().replace("\"", "");
    }

    public static String getSurname(JsonObject user) {
        return user.get("surname").toString().replace("\"", "");
    }

    public static String getPassword(JsonObject user) {
        return user.get("password").toString().replace("\"", "");
    }


}