package helpers.pojo.project;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShareProjectRequestPojo {

    private String email;
    private String permission;

    public ShareProjectRequestPojo(String email, String permission) {
        this.email = email;
        this.permission = permission;
    }
}