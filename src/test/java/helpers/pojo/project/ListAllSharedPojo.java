package helpers.pojo.project;

import helpers.pojo.commonClasses.Project;
import helpers.pojo.commonClasses.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListAllSharedPojo {

    public Integer id;
    public User user;
    public Project project;
    public Boolean writePermission;
}