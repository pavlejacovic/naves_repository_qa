package helpers.pojo.project;

import lombok.Getter;

@Getter
public class ShareProjectResponsePojo {

    public Boolean successful;
    public String message;
}
