package helpers.pojo.file;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RenameRequestPojo {

    private String newName;
    private String path;

    public RenameRequestPojo(String path, String newName) {
        this.newName = newName;
        this.path = path;
    }


}