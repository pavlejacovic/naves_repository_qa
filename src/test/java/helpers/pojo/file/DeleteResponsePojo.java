package helpers.pojo.file;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteResponsePojo {
    private Boolean successful;
    private String message;
}
