package helpers.pojo.file;


import helpers.pojo.commonClasses.UploadedFile;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadResponsePojo {
    private Boolean successful;
    private String message;
    private UploadedFile file;
}