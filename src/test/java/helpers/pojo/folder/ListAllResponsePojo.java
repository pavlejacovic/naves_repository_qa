package helpers.pojo.folder;

import helpers.pojo.commonClasses.File;
import helpers.pojo.commonClasses.Folder;
import helpers.pojo.commonClasses.Project;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListAllResponsePojo {
    private List<File> files = null;
    private List<Folder> folders = null;
    private Project project;
}