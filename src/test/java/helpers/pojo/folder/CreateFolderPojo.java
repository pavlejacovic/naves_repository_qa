package helpers.pojo.folder;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class CreateFolderPojo {
    private String path;
    private String folderName;

    public CreateFolderPojo(String path, String folderName) {
        this.path = path;
        this.folderName = folderName;
    }
}
