package helpers.pojo.function;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FunctionStatisticsResponsePojo {
        private String date;
        private String functionName;
        private Integer numberOfExecutions;
    }

