package helpers.pojo.function;
import java.util.List;

import helpers.pojo.commonClasses.Owner;
import helpers.pojo.commonClasses.Project;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListFunctionsResponsePojo {

    public Integer functionId;
    public String functionName;
    public String functionLink;
    public Boolean publicAccess;
    public String trigger;
    public Owner owner;
    public List<Project> projects = null;
}