package helpers.pojo.function;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import helpers.pojo.commonClasses.Owner;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateFunctionResponsePojo {

        private Integer functionId;
        private String functionName;
        private Object functionLink;
        private Boolean publicAccess;
        private String trigger;
        private Object jarFile;
        private Owner owner;
        private List<Integer> projectIds = null;

    }
