package helpers.pojo.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetUserInformationResponsePojo {

    public String email;
    public String name;
    public String surname;
    public Boolean subscribed;
}
