package helpers.pojo.user;

import helpers.pojo.commonClasses.Project;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminGetProjectsResponsePojo {

    public Project project;
    public Integer numberOfShares;
}