package helpers.pojo.user;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangeUserInformationRequestPojo {

        public String name;
        public String surname;
        public String subscribed;
}