package helpers.pojo.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePasswordRequestPojo {

    public String oldPassword;
    public String newPassword;
}
