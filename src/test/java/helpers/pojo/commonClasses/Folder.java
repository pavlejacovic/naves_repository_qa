package helpers.pojo.commonClasses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Folder {
    private String folderPath;
    private Integer folderSize;
    private String folderName;
}
