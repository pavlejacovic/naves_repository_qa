package helpers.pojo.commonClasses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParentProject {
    private Integer id;
    private Owner owner;
    private String projectName;
    private Integer numberOfFiles;
    private Integer projectSize;
}