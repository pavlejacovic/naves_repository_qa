package helpers.pojo.commonClasses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Owner {
    private String email;
    public Integer userId;
    public Integer type;
}
