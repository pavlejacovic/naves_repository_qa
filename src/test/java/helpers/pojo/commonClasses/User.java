package helpers.pojo.commonClasses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    public String email;
    public Integer type;
}
