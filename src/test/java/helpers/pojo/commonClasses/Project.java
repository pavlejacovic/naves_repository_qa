package helpers.pojo.commonClasses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Project {
    private Integer id;
    private Owner owner;
    private String projectName;
    private Integer numberOfFiles;
    private Integer projectSize;
}
