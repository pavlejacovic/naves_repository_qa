package helpers.pojo.commonClasses;

import helpers.pojo.commonClasses.ParentProject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadedFile {
        private String filePath;
        private Integer fileSize;
        private ParentProject parentProject;
        private String uploadedBy;
        private String fileName;
        private String createdDate;
    }

