//Sasa Avramovic
package helpers.dataBase;

import helpers.utils.Property;

import java.sql.*;

public class DataBaseManipulation {

    //prepare database drivers and query
    public static Connection createConnectionToDatabase() throws SQLException {
        return DriverManager.getConnection(Property.getProperty("dbUrl"), Property.getProperty("dbUser"), Property.getProperty("dbPass"));
    }

    public static ResultSet getQueryResult(String queryRequest) throws SQLException {
        return createConnectionToDatabase().createStatement().executeQuery(queryRequest);
    }

    public static void getUpdateResult(String queryRequest) throws SQLException {
         createConnectionToDatabase().createStatement().executeUpdate(queryRequest);
    }

    public static String getFromDatabase(String email, String requestedData) {
        String resultString = null;
        try {
            ResultSet result = getQueryResult("SELECT " + requestedData + " FROM naves.user WHERE email = \"" + email + "\"");
            result.next();
            resultString = result.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultString;
    }

    public static void deleteUser(String email){
        try {
            Statement st = createConnectionToDatabase().createStatement();
            st.addBatch("DELETE FROM naves.user WHERE email = \"" + email + "\";");
            st.addBatch("DELETE FROM naves2.file WHERE uploaded_by =\"" + email + "\";");
            st.addBatch("DELETE FROM naves2.project WHERE owner_email =\"" + email + "\";");
            st.addBatch("DELETE FROM naves2.registered_user WHERE email =\"" + email + "\";");
            st.addBatch("DELETE FROM naves3.registered_user WHERE email =\"" + email + "\";");
            st.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkProject(String email, String projectName){
        boolean check = false;
        try {
            ResultSet result = getQueryResult("SELECT * FROM naves2.project WHERE owner_email = \"" + email + "\" AND project_name = \"" + projectName + "\";");
            if (result.next()) {
                check = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;
    }

    public static int getProjectID(String email, String projectName){
        int projectID = 0;
        try {
            ResultSet result = getQueryResult("SELECT id FROM naves2.project WHERE owner_email = \"" + email + "\" AND project_name = \"" + projectName + "\";");
            if (result.next())
            projectID = result.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectID;
    }

    public static int getNavesFunctionID(String email, String functionName){
        int functionID = 0;
        int ownerID = Integer.parseInt(getFromDatabase(email, "user_id"));
        try {
            ResultSet result = getQueryResult("SELECT function_id FROM naves3.lambda_function WHERE function_name = \"" + functionName + "-" + ownerID + "\";");
            if (result.next())
                functionID = result.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return functionID;
    }

    public static String getNavesFunctionLink(String email, String functionName){
        String functionLink = "";
        int ownerID = Integer.parseInt(getFromDatabase(email, "user_id"));
        try {
            ResultSet result = getQueryResult("SELECT function_link FROM naves3.lambda_function WHERE function_name = \"" + functionName + "-" + ownerID + "\";");
            if (result.next())
                functionLink = result.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return functionLink;
    }

    public static int checkFiles(String email, int tempProjId) throws SQLException {
        ResultSet result = getQueryResult("SELECT number_of_files FROM naves2.project WHERE owner_email = \"" + email + "\" AND id LIKE \"%" + tempProjId + "%\";");
        result.next();
        return result.getInt(1);
    }

    public static boolean checkFunction(String email, String functionName){
        boolean check = false;
        try {
            ResultSet result = getQueryResult("SELECT * FROM naves3.lambda_function WHERE function_name = \"" + functionName + "-" + getFromDatabase(email, "user_id") + "\"");
            if (result.next()) {
                check = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;
    }

    public static int getLastExecutionID (String email, String functionName)  throws SQLException{
        int lastExecution;
        ResultSet result = getQueryResult("SELECT MAX(execution_id) FROM naves3.execution");
        result.next();
        return result.getInt(1);
    }


    public static void changeDateOfExecution(String email, String functionName) throws SQLException {
        int lastExec = getLastExecutionID(email, functionName);
        boolean check = false;
        try {
            getUpdateResult("UPDATE naves3.execution SET execution_timestamp = '2021-09-20T14:19:27.709599400' WHERE execution_id = " + lastExec);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}