package selenium.ui.box;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import helpers.utils.Actions;
import helpers.utils.Constants;
import helpers.utils.Property;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class BoxFiles extends BaseUI {

    @BeforeClass
    public void loginWithRandomUser(){
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
    }

    @BeforeMethod
    public void prepareForTests() {
        driver.navigate().to(Property.getProperty("mainPageUri"));
        randomProjectName = GenerateRandom.randomDataName();
        randomFolderName = GenerateRandom.randomDataName();
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        ProjectsPage.openProjectOrFolder(driver, wait, randomFolderName);
    }

    @Test
    public void shouldUploadFiles() throws IOException {
        File[] files = BoxFile.createRandomFiles(10);
        driver.findElement(ProjectsPage.projects_upload_button).click();
        StringBuilder sb = new StringBuilder();
        for (File file : files)
            driver.findElement(ProjectsPage.projects_uploadFile_input).sendKeys(file.getAbsolutePath());
        driver.findElement(ProjectsPage.projects_uploadUpload_button).click();
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        for (File file : files) {
            Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.findProjectElement(file.getName()));
            sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(file.getName())).isDisplayed());
        }
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @Test
    public void shouldNotUploadMoreThanTenFiles() throws IOException {
        File[] files = BoxFile.createRandomFiles(11);
        ProjectsPage.uploadFiles(driver, files);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.getProjects_toastWarning_message);
        sa.assertTrue(driver.findElement(ProjectsPage.getProjects_toastWarning_message).isDisplayed());
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @Test
    public void shouldRenameFile() throws IOException {
        File[] files = BoxFile.createRandomFiles(1);
        ProjectsPage.uploadFiles(driver, files);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(files[0].getName())).isDisplayed());
        String randomFileName = GenerateRandom.randomDataName();
        ProjectsPage.renameFile(driver, wait, files[0], randomFileName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFileName)).isDisplayed());
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @Test
    public void shouldNotRenameFileExistingFileName() throws IOException {
        File[] files = BoxFile.createRandomFiles(2);
        ProjectsPage.uploadFiles(driver, files);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        for (File file : files)
            sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(file.getName())).isDisplayed());
        ProjectsPage.renameFile(driver, wait, files[0], FilenameUtils.removeExtension((files[1].getName())));
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        for (File file : files)
            sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(file.getName())).isDisplayed());
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @Test
    public void shouldNotRenameFileInvalidName() throws IOException {
        File[] files = BoxFile.createRandomFiles(1);
        ProjectsPage.uploadFiles(driver, files);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_dropdown_button);
        String randomFileName = "";
        ProjectsPage.renameFile(driver, wait, files[0], randomFileName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_blankName_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_blankName_message).isDisplayed());
        randomFileName = "/";
        ProjectsPage.renameFile(driver, wait, files[0], randomFileName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        randomFileName = "\\";
        ProjectsPage.renameFile(driver, wait, files[0], randomFileName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(files[0].getName())).isDisplayed());
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @AfterMethod
    public void deleteProject(){
        BoxProject.deleteProject(token, DataBaseManipulation.getProjectID(email, randomProjectName));
    }
}
