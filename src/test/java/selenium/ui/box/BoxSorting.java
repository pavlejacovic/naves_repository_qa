package selenium.ui.box;

import com.google.common.collect.Ordering;
import com.google.gson.JsonObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pageObjects.ProjectsPage;
import helpers.generators.GenerateRandom;
import helpers.utils.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import rest.box.project.CreateProject;
import selenium.ui.BaseUI;
import java.io.RandomAccessFile;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class BoxSorting extends BaseUI {
    public JsonObject projectBody = new JsonObject();
    boolean sorted;
    boolean reversed;
    List<WebElement> numberOfProjects = new ArrayList<>();
    List<WebElement> projectsOtN = new ArrayList<>();
    List<WebElement> projectsNtO = new ArrayList<>();
    List<WebElement> projectsNameAsc = new ArrayList<>();
    List<WebElement> projectsNameDesc = new ArrayList<>();
    List<WebElement> projectsSizeAsc = new ArrayList<>();
    List<WebElement> projectsSizeDesc = new ArrayList<>();
    List<String> projectNames = new ArrayList<>();
    List<String> orderedNames = new ArrayList<>();
    List<String> reversedNames = new ArrayList<>();
    ArrayList <Boolean> checkUpload = new ArrayList<>();
    String[] randomProjectNames;


    @BeforeClass
    public void loginWithRandomUser(){
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
    }

    @Test
    public void sorting() throws IOException {
        File[] files = BoxFile.createRandomFiles(2);
        File[] files1 = BoxFile.createRandomFiles(1);

        RandomAccessFile file = new RandomAccessFile(files[0], "rw");
        RandomAccessFile file1 = new RandomAccessFile(files[1], "rw");
        RandomAccessFile file2 = new RandomAccessFile(files1[0], "rw");
        files[0].deleteOnExit();
        files[1].deleteOnExit();
        files1[0].deleteOnExit();
        file.setLength(10485760);
        file1.setLength(5242880);
        file2.setLength(5242880);
        randomProjectNames = new String[4];
        //creating projects and uploading file to check sorting by size
        for(int i = 0; i < 4; i++) {
            randomProjectName = GenerateRandom.randomDataName();
            ProjectsPage.createNewProject(driver, wait, randomProjectName);
            projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
            if (i == 0) BoxFile.uploadFiles(files, "", projectID, token, checkUpload);
            if (i == 1) BoxFile.uploadFiles(files1, "", projectID, token, checkUpload);
            Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.findProjectElement(randomProjectName));
            randomProjectNames[i] = randomProjectName;
        }

        //sorting by date of creation
        ProjectsPage.sortBy(driver, ProjectsPage.oldToNew);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        projectsOtN = driver.findElements(ProjectsPage.projects_projectsClass);
        ProjectsPage.sortBy(driver, ProjectsPage.newToOld);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        projectsNtO = driver.findElements(ProjectsPage.projects_projectsClass);
        //checking if old to new sort is reversed new to old sort
        for(int i=0, j= projectsNtO.size()-1; i<=j; i++,j--){
            sa.assertEquals(projectsNtO.get(i).getAttribute("title"), projectsOtN.get(j).getAttribute("title"));
        }

        //sorting by name
        ProjectsPage.sortBy(driver, ProjectsPage.nameAsc);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        projectsNameAsc = driver.findElements(ProjectsPage.projects_projectsClass);
        ProjectsPage.sortBy(driver, ProjectsPage.nameDesc);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        projectsNameDesc = driver.findElements(ProjectsPage.projects_projectsClass);
        for(int i = 0; i < 4; i++) {
            orderedNames.add(projectsNameAsc.get(i).getAttribute("title"));
            reversedNames.add(projectsNameDesc.get(i).getAttribute("title"));
        }
        sa.assertTrue(Ordering.natural().isOrdered(orderedNames));
        sa.assertTrue(Ordering.natural().reverse().isOrdered(reversedNames));

        //sorting by size
        ProjectsPage.sortBy(driver, ProjectsPage.sizeDesc);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        projectsSizeDesc = driver.findElements(ProjectsPage.projects_projectsClass);
        ProjectsPage.sortBy(driver, ProjectsPage.sizeAsc);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        projectsSizeAsc = driver.findElements(ProjectsPage.projects_projectsClass);
        //checking if two projects with uploaded files (size not null) are sorted accordingly with two empty projects
        sa.assertEquals(projectsSizeDesc.get(0).getAttribute("title"), projectsSizeAsc.get(3).getAttribute("title"));
        sa.assertEquals(projectsSizeDesc.get(1).getAttribute("title"), projectsSizeAsc.get(2).getAttribute("title"));
        sa.assertAll();
    }

    @Test
    public void numberOfProjectsPerPage(){
        randomProjectNames = new String[50];
        for(int i = 0; i < 49; i++) {
            randomProjectName = GenerateRandom.randomDataName();
            projectBody.addProperty("projectName", randomProjectName);
            BoxProject.createProject(projectBody, token);
            randomProjectNames[i] = randomProjectName;
        }

        driver.navigate().refresh();
        ProjectsPage.projectsPerPage(driver, ProjectsPage.projects_12perPage_button);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        numberOfProjects = driver.findElements(ProjectsPage.projects_projectsClass);
        sa.assertEquals(numberOfProjects.size(), 12);

        ProjectsPage.projectsPerPage(driver, ProjectsPage.projects_24perPage_button);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        numberOfProjects = driver.findElements(ProjectsPage.projects_projectsClass);
        sa.assertEquals(numberOfProjects.size(), 24);

        ProjectsPage.projectsPerPage(driver, ProjectsPage.projects_48perPage_button);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_projectsClass);
        numberOfProjects = driver.findElements(ProjectsPage.projects_projectsClass);
        sa.assertEquals(numberOfProjects.size(), 48);
        sa.assertAll();
    }

    @AfterMethod
    public void deleteProjects(){
        for (String project : randomProjectNames) BoxProject.deleteProject(token, DataBaseManipulation.getProjectID(email, project));
    }
}