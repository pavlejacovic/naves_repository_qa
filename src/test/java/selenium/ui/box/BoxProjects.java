package selenium.ui.box;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import helpers.utils.Actions;
import helpers.utils.Constants;
import helpers.utils.Property;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class BoxProjects extends BaseUI{

    String randomProjectRename;

    @BeforeClass
    public void loginWithRandomUser(){
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
    }

    @BeforeMethod
    public void prepareForTests() {
        driver.navigate().to(Property.getProperty("mainPageUri"));
        randomProjectName = GenerateRandom.randomDataName();
        randomFolderName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldCreateNewProject(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectBlankName(){
        randomProjectName = "";
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_blankName_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_blankName_message).isDisplayed());
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateNewProjectExistingName(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldDeleteProject(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName));
        ProjectsPage.deleteProjectElement(randomProjectName, wait, driver);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteProject(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        //delete done step-by-step just for this test
        driver.findElement(ProjectsPage.findProjectElement(randomProjectName)).findElement(ProjectsPage.projects_dropdown_button).click();
        driver.findElement(ProjectsPage.projects_dropdownDelete_button).click();
        driver.findElement(ProjectsPage.projects_decline_button).click();
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldRenameProject(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        randomProjectRename = GenerateRandom.randomDataName();
        ProjectsPage.renameProject(driver, wait, randomProjectName, randomProjectRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomProjectRename)).isDisplayed());
        sa.assertTrue(driver.findElements(ProjectsPage.findProjectElement(randomProjectName)).size() < 1);
        sa.assertAll();
    }

    @Test
    public void shouldNorRenameProjectExistingName(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        randomProjectRename = GenerateRandom.randomDataName();
        ProjectsPage.createNewProject(driver, wait, randomProjectRename);
        ProjectsPage.renameProject(driver, wait, randomProjectName, randomProjectRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomProjectName)).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomProjectRename)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectBlankName(){
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        randomProjectRename = "";
        ProjectsPage.renameProject(driver, wait, randomProjectName, randomProjectRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_blankName_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_blankName_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomProjectName)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldDownloadProjectAsZip() throws IOException {
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        File[] files = BoxFile.createRandomFiles(10);
        ArrayList<Boolean> checkUpload = new ArrayList<>();
        randomFolderName = "";
        BoxFile.uploadFiles(files, randomFolderName, DataBaseManipulation.getProjectID(email, randomProjectName), token, checkUpload);
        ProjectsPage.downloadProjectOrFolder(driver, wait, randomProjectName);
        wait.until(tempWait -> new File(String.valueOf(Paths.get(Constants.downloadFolder, randomProjectName + ".zip"))).exists());
        sa.assertTrue(Files.exists(Paths.get(Constants.downloadFolder, randomProjectName + ".zip")));
        Files.deleteIfExists(Paths.get(Constants.downloadFolder, randomFolderName + ".zip"));
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @AfterMethod
    public void deleteProject(){
        BoxProject.deleteProject(token, DataBaseManipulation.getProjectID(email, randomProjectName));
        BoxProject.deleteProject(token, DataBaseManipulation.getProjectID(email, randomProjectRename));
    }
}
