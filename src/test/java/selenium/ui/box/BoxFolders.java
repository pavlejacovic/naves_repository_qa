package selenium.ui.box;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import helpers.utils.Actions;
import helpers.utils.Constants;
import helpers.utils.Property;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;


public class BoxFolders extends BaseUI {

    @BeforeClass
    public void loginWithRandomUser(){
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
    }

    @BeforeMethod
    public void prepareForTests() {
        driver.navigate().to(Property.getProperty("mainPageUri"));
        randomProjectName = GenerateRandom.randomDataName();
        ProjectsPage.createNewProject(driver, wait, randomProjectName);
        randomFolderName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldCreateFolder(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderBlankName(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        randomFolderName = "";
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_blankName_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_blankName_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderInvalidName(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        randomFolderName = "..";
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        ProjectsPage.createNewFolder(driver, wait, "/");
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        ProjectsPage.createNewFolder(driver, wait, "\\");
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderExistingName(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldDeleteFolder(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.findProjectElement(randomFolderName));
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).isDisplayed());
        ProjectsPage.deleteFolder(randomFolderName, wait, driver);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastInfo_message);
        sa.assertTrue(driver.findElements(ProjectsPage.findProjectElement(randomFolderName)).size() < 1);
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteFolder(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.findProjectElement(randomFolderName));
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).isDisplayed());
        //delete done step-by-step just for this test
        driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).findElement(ProjectsPage.projects_dropdown_button).click();
        driver.findElement(ProjectsPage.projects_dropdownDeleteFolder_button).click();
        driver.findElement(ProjectsPage.projects_decline_button).click();
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldRenameFolder(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        String randomFolderRename = GenerateRandom.randomDataName();
        ProjectsPage.renameFolder(driver, wait, randomFolderName, randomFolderRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastSuccess_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderRename)).isDisplayed());
        sa.assertTrue(driver.findElements(ProjectsPage.findProjectElement(randomFolderName)).size() < 1);
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderExistingFolder(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        String randomFolderRename = GenerateRandom.randomDataName();
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderRename);
        ProjectsPage.renameFolder(driver, wait, randomFolderName, randomFolderRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastSuccess_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderRename)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderBlankName(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        String randomFolderRename = "";
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        ProjectsPage.renameFolder(driver, wait, randomFolderName, randomFolderRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_blankName_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_blankName_message).isDisplayed());
        sa.assertTrue(driver.findElement(ProjectsPage.findProjectElement(randomFolderName)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderInvalidName(){
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        String randomFolderRename = "..";
        ProjectsPage.renameFolder(driver, wait, randomFolderName, randomFolderRename);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        randomFolderRename = "/";
        Actions.sendTextToElement(driver, wait, ProjectsPage.projects_inputName_box, randomFolderRename);
        driver.findElement(ProjectsPage.projects_renameFolder_button).click();
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        randomFolderRename = "\\";
        Actions.sendTextToElement(driver, wait, ProjectsPage.projects_inputName_box, randomFolderRename);
        driver.findElement(ProjectsPage.projects_renameFolder_button).click();
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_toastError_message);
        sa.assertTrue(driver.findElement(ProjectsPage.projects_toastError_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldDownloadFolderAsZip() throws IOException {
        ProjectsPage.openProjectOrFolder(driver, wait, randomProjectName);
        ProjectsPage.createNewFolder(driver, wait, randomFolderName);
        File[] files = BoxFile.createRandomFiles(10);
        ArrayList<Boolean> checkUpload = new ArrayList<>();
        BoxFile.uploadFiles(files, randomFolderName, DataBaseManipulation.getProjectID(email, randomProjectName), token, checkUpload);
        ProjectsPage.downloadProjectOrFolder(driver, wait, randomFolderName);
        wait.until(tempWait -> new File(String.valueOf(Paths.get(Constants.downloadFolder, randomFolderName + ".zip"))).exists());
        sa.assertTrue(Files.exists(Paths.get(Constants.downloadFolder, randomFolderName + ".zip")));
        Files.deleteIfExists(Paths.get(Constants.downloadFolder, randomFolderName + ".zip"));
        sa.assertAll();
        for (File file : files) file.deleteOnExit();
    }

    @AfterMethod
    public void deleteProject(){
        BoxProject.deleteProject(token, DataBaseManipulation.getProjectID(email, randomProjectName));
    }
}
