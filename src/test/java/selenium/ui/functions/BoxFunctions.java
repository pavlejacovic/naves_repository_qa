package selenium.ui.functions;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.DockerObject;
import com.google.gson.JsonObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import helpers.requests.NavesFunction;
import helpers.utils.Actions;
import helpers.utils.Property;
import helpers.utils.UtilsForAPI;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testcontainers.DockerClientFactory;
import org.awaitility.Awaitility;
import org.testng.annotations.*;
import pageObjects.FunctionsPage;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Callable;
import java.awt.event.ContainerListener;
import org.testcontainers.DockerClientFactory;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.DockerStatus;

public class BoxFunctions extends BaseUI {

    Integer[] prID = new Integer[1];
    String randomFunctionName;
    File jarFile;
    File zipFile;
    JsonObject projectBody = new JsonObject();
    String parameter = "name";
    String value = "value";
    String method = "GET";
    String contentType = "multipart/form-data";
    String triggerPost = "POST";
    String triggerGet = "GET";
    String triggerBucket = "BUCKET";
    String javaLang = "java";
    String csharpLang = "c#";
    String publicAccess = "PUBLIC";
    DockerClient dockerClient = DockerClientFactory.instance().client();
    List<Container> allContainers = new ArrayList<>();

    @BeforeClass
    public void loginWithRandomUserAndCreateProject() {
        randomProjectName = GenerateRandom.randomDataName();
        projectBody.addProperty("projectName", randomProjectName);
        BoxProject.createProject(projectBody, token);
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
        prID[0] = DataBaseManipulation.getProjectID(email, randomProjectName);
        driver.navigate().to(Property.getProperty("functionsUri"));
    }

    @BeforeMethod
    public void setRandomFunctionName() throws IOException {
        randomFunctionName = GenerateRandom.randomDataName();
        jarFile = BoxFile.createRandomFile(".jar");
        zipFile = BoxFile.createRandomFile(".zip");
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_createFunction_button);
    }

    @Test
    public void shouldCreateFunction()  {
        FunctionsPage.createOrEditProperties(driver, wait, randomFunctionName, null, javaLang, jarFile, triggerGet, publicAccess, null);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.findFunctionElement(randomFunctionName));
        sa.assertTrue(driver.findElement(FunctionsPage.findFunctionElement(randomFunctionName)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldCreateFunctionBucket() {
        FunctionsPage.createOrEditProperties(driver, wait, randomFunctionName, null, javaLang, jarFile, triggerBucket, "DOWNLOAD", randomProjectName);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.findFunctionElement(randomFunctionName));
        sa.assertTrue(driver.findElement(FunctionsPage.findFunctionElement(randomFunctionName)).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldUpdateFunctionExecutionJava() {
        NavesFunction.createNavesFunction(token, randomFunctionName, true, triggerGet, jarFile, language, prID);
        driver.navigate().refresh();
        FunctionsPage.updateFuncExe(driver, wait, randomFunctionName, javaLang, jarFile);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_toastSuccess_message);
        sa.assertTrue(driver.findElement(FunctionsPage.functions_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldUpdateFunctionExecutionCSharp() {
        NavesFunction.createNavesFunction(token, randomFunctionName, true, triggerGet, jarFile, language, prID);
        driver.navigate().refresh();
        FunctionsPage.updateFuncExe(driver, wait, randomFunctionName, csharpLang, zipFile);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_toastSuccess_message);
        sa.assertTrue(driver.findElement(FunctionsPage.functions_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldDeleteFunction() {
        NavesFunction.createNavesFunction(token, randomFunctionName, true, triggerGet, jarFile, language, prID);
        driver.navigate().refresh();
        FunctionsPage.deleteFunction(driver, wait, randomFunctionName);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_toastSuccess_message);
        sa.assertTrue(driver.findElement(FunctionsPage.functions_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldUpdateFunctionProperties(){
        NavesFunction.createNavesFunction(token, randomFunctionName, true, triggerGet, jarFile, language, prID);
        driver.navigate().refresh();
        FunctionsPage.createOrEditProperties(driver, wait, randomFunctionName, "new-name", null, jarFile, triggerPost, publicAccess, null);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_toastSuccess_message);
        sa.assertTrue(driver.findElement(FunctionsPage.functions_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldGetStatistics() {
        File jarFile = new File("jarsForTests/Wait.jar");
        NavesFunction.createNavesFunction(token, randomFunctionName, true, triggerGet, jarFile, language, prID);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        String link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        NavesFunction.executeFunction(token, parameter, value, method, link, contentType);
        driver.navigate().refresh();
        FunctionsPage.getStatistics(driver, wait, randomFunctionName, 2);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_statisticChart_class);
        sa.assertTrue(driver.findElement(FunctionsPage.functions_statisticChart_class).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldGenerateNewLinkAndExecute() throws AWTException{
        File jarFile = new File("jarsForTests/Wait.jar");
        FunctionsPage.createOrEditProperties(driver, wait, randomFunctionName, null, javaLang, jarFile, triggerGet, publicAccess, null);
        Actions.waitForElementToBeVisible(driver, wait, FunctionsPage.functions_toastSuccess_message);
        FunctionsPage.openNewTabAndExecuteFunction(driver, wait, randomFunctionName);
        Awaitility.await().until(UtilsForAPI.containerStarted(dockerClient, allContainers, email, randomFunctionName));
        sa.assertTrue(allContainers.get(0).getImage().equals(randomFunctionName + "-" + DataBaseManipulation.getFromDatabase(email, "user_id")));
        sa.assertAll();
    }

    @AfterMethod
    public void deleteFunction(){
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, "new-name"));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
    }

    @AfterClass
    public void deleteFile() {
        jarFile.deleteOnExit();
        BoxProject.deleteProject(token, DataBaseManipulation.getProjectID(email, randomProjectName));
    }
}