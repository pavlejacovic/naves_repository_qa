package selenium.ui.user;

import helpers.Constants;
import helpers.utils.Actions;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;

import io.restassured.RestAssured;

public class UserRouting extends BaseUI {

    @Test
    public void NotLoggedIn(){
        driver.get(Property.getProperty("frontUri"));
        driver.navigate().refresh();
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("loginUri"), "frontUrl");
        driver.get(Property.getProperty("loginUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("loginUri"), "url_login");
        driver.get(Property.getProperty("resetPasswordUri"));
        sa.assertEquals(driver.getCurrentUrl(), (Property.getProperty("resetPasswordUri")), "url_resetPassword");
        driver.get((Property.getProperty("forgotPassUri")));
        sa.assertEquals(driver.getCurrentUrl(), (Property.getProperty("forgotPassUri")), "url_forgotPassword");
        driver.get(Property.getProperty("registerUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("registerUri"), "url_register");
        driver.get(Property.getProperty("frontUri") + "confirm-account");
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("registerUri"), "confirm-account");
        driver.get(Property.getProperty("frontUri") + "oauth");
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("loginUri"), "oauth");
        driver.get(Property.getProperty("frontUri") + "randomString");
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("loginUri"), "randomString");
        sa.assertAll();
        DataBaseManipulation.deleteUser(email);
    }

    @Test
    public void LoggedIn(){
        driver.get(Property.getProperty("frontUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("loginUri"), "login page");
        LoginPage.login(email, Constants.validPasswordEightChars, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("frontUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("registerUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("forgotPassUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("loginUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("frontUri") + "confirm-account");
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("frontUri") + "oauth");
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("resetPasswordUri"));
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        driver.get(Property.getProperty("frontUri") + "randomString");
        sa.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
        sa.assertAll();
        DataBaseManipulation.deleteUser(email);
    }
}
