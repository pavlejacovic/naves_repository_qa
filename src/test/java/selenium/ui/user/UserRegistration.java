//Sasa Avramovic
package selenium.ui.user;

import helpers.Constants;
import helpers.utils.Actions;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import helpers.dataBase.DataBaseManipulation;
import pageObjects.RegistrationPage;
import helpers.generators.UserBuilder;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import selenium.ui.BaseUI;

public class UserRegistration extends BaseUI {

    @BeforeMethod
    public void setupUser() {
        driver.get(Property.getProperty("registerUri"));
        testUser = UserBuilder.testUser();
    }

    @Test
    public void shouldRegisterWithSubscription() {
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_confirmation_text);
        //check if confirmation message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_confirmation_text).isDisplayed());
        //check from database if user is subscribed
        sa.assertEquals(DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "subscribed"), "1");
        sa.assertAll();
    }

    @Test
    public void shouldRegisterWithoutSubscription() {
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.unsubscribe(driver, wait);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_confirmation_text);
        //check if confirmation message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_confirmation_text).isDisplayed());
        //check from database if user is subscribed
        sa.assertEquals(DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "subscribed"), "0");
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithoutFirstName() {
        testUser.addProperty("name", "");
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithoutLastName() {
        testUser.addProperty("surname", "");
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithoutEmail() {
        testUser.addProperty("email", "");
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithoutPassword() {
        testUser.addProperty("password", "");
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithInvalidEmailNoDomain() {
        testUser.addProperty("email", Constants.invalidEmailNoDomain);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithInvalidEmailDotFirst() {
        testUser.addProperty("email", Constants.invalidEmailDotFirst);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithInvalidEmailDotLast() {
        testUser.addProperty("email", Constants.invalidEmailDotLast);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterWithInvalidEmailTwoDots() {
        testUser.addProperty("email", Constants.invalidEmailTwoDots);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterPasswordWithoutSpecial() {
        testUser.addProperty("password", Constants.invalidPasswordNoSpecial);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterPasswordWithoutCaps() {
        testUser.addProperty("password", Constants.invalidPasswordNoSpecial);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();

    }

    @Test
    public void shouldNotRegisterPasswordWithoutNumber() {
        testUser.addProperty("password", Constants.invalidPasswordNoNumber);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterPasswordAllCaps() {
        testUser.addProperty("password", Constants.invalidPasswordAllCaps);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotRegisterPasswordTooShort() {
        testUser.addProperty("password", Constants.invalidPasswordSevenChars);
        RegistrationPage.fillInRegistration(driver, wait, testUser);
        RegistrationPage.register(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, RegistrationPage.register_errorMessage_text);
        //check if error message appears
        sa.assertTrue(driver.findElement(RegistrationPage.register_errorMessage_text).isDisplayed());
        sa.assertAll();
    }

    @AfterMethod
    public void deleteUser() {
        DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(testUser));
    }
}