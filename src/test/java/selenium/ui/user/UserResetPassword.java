package selenium.ui.user;
// Boris Nedeljkovic
// Sasa Avramovic

import helpers.Constants;
import pageObjects.ResetPasswordPage;
import helpers.dataBase.DataBaseManipulation;
import helpers.requests.Authentication;
import helpers.utils.Actions;
import helpers.utils.Property;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import selenium.ui.BaseUI;
import io.restassured.RestAssured;

public class UserResetPassword extends BaseUI {

    //user is made in userBuilder and is a randomly generated email with repeating password
    //BeforeMethod sends password reset email with link that contains resetPassword token, which is then pulled from DB
    @BeforeMethod
    public void registerAndSendForgotPasswordEmail() {
        //sends email with link to change password
        Authentication.sendResetPasswordEmail(email);
        //navigates to reset password page
        driver.navigate().to(Property.getProperty("resetPasswordUri") + DataBaseManipulation.getFromDatabase(email, "reset_password_token"));
    }

    //Testing resetting of password  with the user being verified and email for new password sent in the before method
    @Test
    public void shouldResetPassword() {
        ResetPasswordPage.changePassword(password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_successfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_successfulChange_message).isDisplayed());
    }

    //Testing resetting of password with empty body
    @Test
    public void shouldNotResetPasswordInvalidLink() {
        driver.get(Property.getProperty("resetPasswordUri") + "randomstring");
        ResetPasswordPage.changePassword(password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.projects_toastError_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.projects_toastError_message).isDisplayed());
    }

    //Testing resetting of password with password that is too short
    @Test
    public void shouldNotResetPasswordTooShort() {
        ResetPasswordPage.changePassword(Constants.invalidPasswordSevenChars, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }

    //Testing resetting of password with password that has no small letters
    @Test
    public void shouldNotResetPasswordAllCaps() {
        ResetPasswordPage.changePassword(Constants.invalidPasswordAllCaps, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }

    //Testing resetting of password with password that has no capital letters
    @Test
    public void shouldNotResetPasswordNoCaps() {
        ResetPasswordPage.changePassword(Constants.invalidPasswordNoCapitalLetter,  driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }

    //Testing resetting of password with password that has no numbers
    @Test
    public void shouldNotResetPasswordNoNumber() {
        ResetPasswordPage.changePassword(Constants.invalidPasswordNoNumber, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }

    //Testing resetting of password with password that has no special characters
    @Test
    public void shouldNotResetPasswordNoSpecial() {
        ResetPasswordPage.changePassword(Constants.invalidPasswordNoSpecial, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }

    //Testing resetting of password with password that is empty
    @Test
    public void shouldNotResetPasswordEmptyPassword() {
        ResetPasswordPage.inputNewPassword("", driver, wait);
        ResetPasswordPage.inputConfirmNewPassword(password, driver, wait);
        ResetPasswordPage.clickOnSaveNewPassword(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());

    }

    //Testing resetting of password with password confirmation that is empty
    @Test
    public void shouldNotResetPasswordEmptyRepeatPassword() {
        ResetPasswordPage.inputNewPassword(password, driver, wait);
        ResetPasswordPage.inputConfirmNewPassword("", driver, wait);
        ResetPasswordPage.clickOnSaveNewPassword(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }

    //Testing resetting of password with password and confirm password that are empty
    @Test
    public void shouldNotResetPasswordEmptyBothPasswords() {
        ResetPasswordPage.changePassword("", driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ResetPasswordPage.resetPassword_unsuccessfulChange_message);
        Assert.assertTrue(driver.findElement(ResetPasswordPage.resetPassword_unsuccessfulChange_message).isDisplayed());
    }
}






