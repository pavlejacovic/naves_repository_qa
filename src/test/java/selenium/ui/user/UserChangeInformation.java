package selenium.ui.user;

import helpers.Constants;
import helpers.utils.Actions;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.ChangeInformationPage;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;

public class UserChangeInformation extends BaseUI {
    String newPassNineChars = Constants.validPasswordNineChars;
    String newPassMaxChars = Constants.validPasswordMaxChars;
    String oldPass;
    @BeforeClass
    public void loginWithRandomUserAndCreateProject() {
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
        driver.navigate().to(Property.getProperty("userProfilePage"));
    }

    @BeforeMethod
    public void getPassword(){
        oldPass = GetUserPropertyFromJson.getPassword(testUser);
    }

    @Test
    public void shouldChangeUserInfo() {
        ChangeInformationPage.changeUserInfo(driver, wait, "user", "password");
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_toastSuccess_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldChangePassword() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.changeUserPassword(driver, wait, oldPass, newPassNineChars, newPassNineChars);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_toastSuccess_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldChangePasswordMaxChars() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.changeUserPassword(driver, wait, oldPass, newPassMaxChars, newPassMaxChars);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_toastSuccess_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_toastSuccess_message).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidNoSpecChars() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.changeUserPassword(driver, wait, oldPass, Constants.invalidPasswordNoSpecial, Constants.invalidPasswordNoSpecial);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.resetPassword_unsuccessfulChange_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.resetPassword_unsuccessfulChange_message).isDisplayed());
        ChangeInformationPage.clickCancel(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_editInformation_button);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_editInformation_button).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidNoNumber() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.changeUserPassword(driver, wait, oldPass, Constants.invalidPasswordNoNumber, Constants.invalidPasswordNoNumber);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.resetPassword_unsuccessfulChange_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.resetPassword_unsuccessfulChange_message).isDisplayed());
        ChangeInformationPage.clickCancel(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_editInformation_button);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_editInformation_button).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidNoCapitalLetter() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.changeUserPassword(driver, wait, oldPass, Constants.invalidPasswordNoCapitalLetter, Constants.invalidPasswordNoCapitalLetter);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.resetPassword_unsuccessfulChange_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.resetPassword_unsuccessfulChange_message).isDisplayed());
        ChangeInformationPage.clickCancel(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_editInformation_button);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_editInformation_button).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidNoSmallLetter() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.changeUserPassword(driver, wait, oldPass, Constants.invalidPasswordAllCaps, Constants.invalidPasswordAllCaps);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.resetPassword_unsuccessfulChange_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.resetPassword_unsuccessfulChange_message).isDisplayed());
        ChangeInformationPage.clickCancel(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_editInformation_button);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_editInformation_button).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidEmptyConfirm() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.inputOldPassword(driver, wait, oldPass);
        ChangeInformationPage.inputNewPassword(driver, wait, newPassNineChars);
        ChangeInformationPage.inputConfirmPassword(driver, wait, "");
        ChangeInformationPage.clickOnSubmit(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.resetPassword_unsuccessfulChange_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.resetPassword_unsuccessfulChange_message).isDisplayed());
        ChangeInformationPage.clickCancel(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_editInformation_button);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_editInformation_button).isDisplayed());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidEmptyNew() {
        ChangeInformationPage.clearPasswordField(driver, wait);
        ChangeInformationPage.inputOldPassword(driver, wait, oldPass);
        ChangeInformationPage.inputNewPassword(driver, wait, "");
        ChangeInformationPage.inputConfirmPassword(driver, wait, newPassNineChars);
        ChangeInformationPage.clickOnSubmit(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.resetPassword_unsuccessfulChange_message);
        sa.assertTrue(driver.findElement(ChangeInformationPage.resetPassword_unsuccessfulChange_message).isDisplayed());
        ChangeInformationPage.clickCancel(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ChangeInformationPage.userInfo_editInformation_button);
        sa.assertTrue(driver.findElement(ChangeInformationPage.userInfo_editInformation_button).isDisplayed());
        sa.assertAll();
    }
}
