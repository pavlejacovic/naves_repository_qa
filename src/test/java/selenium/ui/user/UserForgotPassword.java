package selenium.ui.user;
// Boris Nedeljkovic

import helpers.Constants;
import helpers.utils.Actions;
import helpers.utils.Property;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.ForgotPasswordPage;
import selenium.ui.BaseUI;
import io.restassured.RestAssured;

public class UserForgotPassword extends BaseUI {

    @BeforeMethod
    public void goToPage(){
        driver.get(Property.getProperty("forgotPassUri"));
    }

    //Testing if forgot password feature sends verification email (no matter if it is in DB or not)
    @Test
    public void shouldSendVerificationEmail() {
        ForgotPasswordPage.sendResetPasswordEmail(email, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ForgotPasswordPage.forgotPassword_successfulEmailInput_message_class);
        Assert.assertTrue(driver.findElement(ForgotPasswordPage.forgotPassword_successfulEmailInput_message_class).isDisplayed());
    }

    //Testing if forgot password feature sends verification email if email is wrong format
    @Test
    public void invalidEmailFormat() {
        invalidEmail = Constants.invalidEmailDotLast;
        ForgotPasswordPage.sendResetPasswordEmail(invalidEmail, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ForgotPasswordPage.forgotPassword_invalidEmail_text);
        Assert.assertTrue(driver.findElement(ForgotPasswordPage.forgotPassword_invalidEmail_text).isDisplayed());
    }

    //Testing if forgot password feature sends verification email if email bar is empty
    @Test
    public void invalidEmptyEmail() {
        ForgotPasswordPage.sendResetPasswordEmail("", driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ForgotPasswordPage.forgotPassword_invalidEmail_text);
        Assert.assertTrue(driver.findElement(ForgotPasswordPage.forgotPassword_invalidEmail_text).isDisplayed());
    }
}