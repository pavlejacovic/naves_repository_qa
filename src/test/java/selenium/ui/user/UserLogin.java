package selenium.ui.user;

import helpers.Constants;
import helpers.dataBase.DataBaseManipulation;
import helpers.utils.Actions;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pageObjects.LoginPage;
import pageObjects.ProjectsPage;
import selenium.ui.BaseUI;

import io.restassured.RestAssured;

public class UserLogin extends BaseUI {

    //Test should login successfully
    @Test
    public void shouldLogin() {
        LoginPage.login(email, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, ProjectsPage.projects_signOut_button);
        Assert.assertEquals(driver.getCurrentUrl(), Property.getProperty("mainPageUri"));
    }

    //Testing login with empty email
    @Test
    public void invalidLoginEmptyEmail() {
        LoginPage.login("", password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, LoginPage.login_incorrectEmailOrPassword_text);
        Assert.assertTrue(driver.findElement(LoginPage.login_incorrectEmailOrPassword_text).isDisplayed());
    }

    //Testing login with empty password
    @Test
    public void invalidLoginEmptyPassword() {
        LoginPage.login(email, "", driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, LoginPage.login_incorrectEmailOrPassword_text);
        Assert.assertTrue(driver.findElement(LoginPage.login_incorrectEmailOrPassword_text).isDisplayed());
    }

    //Testing login with wrong password
    @Test
    public void invalidLoginWrongPassword() {
        LoginPage.login(email, Constants.invalidPasswordSevenChars, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, LoginPage.login_incorrectEmailOrPassword_text);
        Assert.assertTrue(driver.findElement(LoginPage.login_incorrectEmailOrPassword_text).isDisplayed());
    }

    //Testing login with wrong email
    @Test
    public void invalidLoginWrongEmail() {
        invalidEmail = Constants.invalidEmailDotFirst;
        LoginPage.login(invalidEmail, password, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, LoginPage.login_incorrectEmailOrPassword_text);
        Assert.assertTrue(driver.findElement(LoginPage.login_incorrectEmailOrPassword_text).isDisplayed());
    }

    //Testing login with unverified user
    @Test
    public void invalidLoginUserNotVerified() {
        testUser1 = UserBuilder.testUser();
        email1 = GetUserPropertyFromJson.getEmail(testUser1);
        password1 = GetUserPropertyFromJson.getPassword(testUser1);
        Authentication.registerUser(testUser1);
        LoginPage.login(email1, password1, driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, LoginPage.login_incorrectEmailOrPassword_text);
        Assert.assertTrue(driver.findElement(LoginPage.login_incorrectEmailOrPassword_text).isDisplayed());
        DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(testUser1));
    }
}
