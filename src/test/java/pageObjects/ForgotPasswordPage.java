package pageObjects;

import helpers.utils.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForgotPasswordPage {

    public static By forgotPassword_email_box = By.id("email");
    public static By forgotPassword_sendLink_button = By.cssSelector("[data-test-id=\"sendLinkBtn\"]");
    public static By forgotPassword_backToLogin_button = By.xpath("//button[contains(., 'to login')]");
    public static By forgotPassword_invalidEmail_text = By.cssSelector("p[class*='AlertMessage_AlertMessage']");
    public static By forgotPassword_successfulEmailInput_message_class = By.cssSelector("p[class*='ForgotPasswordPage_ForgotPasswordPage-Message']");

    public static void inputEmail(String email, WebDriver driver, WebDriverWait wait) {
        Actions.sendTextToElement(driver, wait, forgotPassword_email_box, email);
    }

    public static void clickOnSendLink(WebDriver driver, WebDriverWait wait) {
        Actions.waitForElementToBeVisible(driver, wait, forgotPassword_sendLink_button);
        driver.findElement(forgotPassword_sendLink_button).click();
        Actions.waitForElementToBeVisible(driver, wait, forgotPassword_successfulEmailInput_message_class);
    }

    public static void sendResetPasswordEmail(String email, WebDriver driver, WebDriverWait wait) {
        inputEmail(email, driver, wait);
        clickOnSendLink(driver, wait);
    }
}
