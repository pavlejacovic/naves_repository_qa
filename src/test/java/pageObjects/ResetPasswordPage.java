package pageObjects;

import helpers.utils.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResetPasswordPage {

    public static By resetPassword_inputPassword_box = By.id("password");
    public static By resetPassword_confirmPassword_box = By.id("confirmPassword");
    public static By resetPassword_submitNewPassword_button = By.cssSelector("[data-test-id=\"saveNewPasswordBtn\"]");
    public static By resetPassword_return_button = By.cssSelector("//button[contains(., 'Back to login')]");
    public static By resetPassword_successfulChange_message = By.xpath("//*[contains(text(),'You have successfully changed your password')]");
    public static By projects_toastError_message = By.cssSelector("div[class*='error']");
    public static By resetPassword_unsuccessfulChange_message = By.cssSelector("p[class*='AlertMessage_AlertMessage_warning']");

    public static void inputNewPassword(String password, WebDriver driver, WebDriverWait wait) {
        Actions.sendTextToElement(driver, wait, resetPassword_inputPassword_box, password);
    }

    public static void inputConfirmNewPassword(String password, WebDriver driver, WebDriverWait wait) {
        Actions.sendTextToElement(driver, wait, resetPassword_confirmPassword_box, password);
    }

    public static void clickOnSaveNewPassword(WebDriver driver, WebDriverWait wait) {
        Actions.waitForElementToBeVisible(driver, wait, resetPassword_submitNewPassword_button);
        driver.findElement(resetPassword_submitNewPassword_button).click();
    }

    public static void changePassword(String password, WebDriver driver, WebDriverWait wait)  {
        inputNewPassword(password, driver, wait);
        inputConfirmNewPassword(password, driver, wait);
        clickOnSaveNewPassword(driver, wait);
    }
}
