package pageObjects;

import com.google.gson.JsonObject;
import helpers.utils.Actions;
import helpers.utils.GetUserPropertyFromJson;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {
    //elements
    public static By register_name_box = By.id("name");
    public static By register_lastName_box = By.id("lastName");
    public static By register_email_box = By.id("email");
    public static By register_password_box = By.id("password");
    public static By register_confirmPassword_box = By.id("confirmPassword");
    public static By register_subscribe_checkbox = By.id("subscribe");
    public static By register_terms_checkbox = By.id("terms-conditions");
    public static By register_signUp_button = By.cssSelector("[data-test-id=\"signUpButton\"]");
    public static By register_login_button = By.partialLinkText("login");
    public static By register_confirmation_text = By.cssSelector("p[class*='RegisterPage_RegisterForm-Response']");
    public static By register_errorMessage_text = By.cssSelector("p[class*='AlertMessage_AlertMessage_warning']");

    public static void inputName(WebDriver driver, WebDriverWait wait, String name){
        Actions.sendTextToElement(driver, wait, register_name_box, name);
    }

    public static void inputLastName(WebDriver driver, WebDriverWait wait, String lastName){
        Actions.sendTextToElement(driver, wait, register_lastName_box, lastName);
    }
    public static void inputEmail(WebDriver driver, WebDriverWait wait, String email){
        Actions.sendTextToElement(driver, wait, register_email_box, email);
    }

    public static void inputPassword(WebDriver driver, WebDriverWait wait, String password){
        Actions.sendTextToElement(driver, wait, register_password_box, password);
    }

    public static void inputConfirmPassword(WebDriver driver, WebDriverWait wait, String confirmPassword){
        Actions.sendTextToElement(driver, wait, register_confirmPassword_box, confirmPassword);
    }

    public static void acceptTerms(WebDriver driver, WebDriverWait wait){
        Actions.checkCheckbox(driver, wait, register_terms_checkbox);
    }

    public static void declineTerms(WebDriver driver, WebDriverWait wait){
        Actions.uncheckCheckbox(driver, wait, register_terms_checkbox);
    }

    public static void subscribe(WebDriver driver, WebDriverWait wait){
        Actions.checkCheckbox(driver, wait, register_subscribe_checkbox);
    }

    public static void unsubscribe(WebDriver driver, WebDriverWait wait){
        Actions.uncheckCheckbox(driver, wait, register_subscribe_checkbox);
    }

    public static void register(WebDriver driver, WebDriverWait wait){
        Actions.waitForElementToBeVisible(driver, wait, register_signUp_button);
        driver.findElement(register_signUp_button).click();
    }

    public static void fillInRegistration(WebDriver driver, WebDriverWait wait, JsonObject testUser){
        inputName(driver, wait, GetUserPropertyFromJson.getName(testUser));
        inputLastName(driver, wait, GetUserPropertyFromJson.getSurname(testUser));
        inputEmail(driver, wait, GetUserPropertyFromJson.getEmail(testUser));
        inputPassword(driver, wait, GetUserPropertyFromJson.getPassword(testUser));
        inputConfirmPassword(driver, wait, GetUserPropertyFromJson.getPassword(testUser));
        subscribe(driver, wait);
        acceptTerms(driver, wait);
    }
}