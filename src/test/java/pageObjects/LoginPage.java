package pageObjects;

import static helpers.utils.Actions.*;
import helpers.utils.Property;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginPage {
    public static By login_email_box = By.id("email");
    public static By login_password_box = By.id("password");
    public static By login_signIn_button = By.cssSelector("button[type=\"submit\"]");
    public static By login_signUp_button = By.partialLinkText("Sign up");
    public static By login_forgotPassword_link = By.partialLinkText("Forgot password");
    public static By login_incorrectEmailOrPassword_text = By.cssSelector("p[class*='AlertMessage_AlertMessage']");

    public static void inputEmail(String email, WebDriver driver, WebDriverWait wait){
        sendTextToElement(driver, wait, login_email_box, email);
    }

    public static void inputPassword(String password, WebDriver driver, WebDriverWait wait){
        sendTextToElement(driver, wait, login_password_box, password);
    }

    public static void clickLogin(WebDriver driver, WebDriverWait wait){
        waitForElementToBeVisible(driver, wait, login_signIn_button);
        driver.findElement(login_signIn_button).click();
    }

    public static void login(String email, String password, WebDriver driver, WebDriverWait wait){
        driver.get(Property.getProperty("loginUri"));
        inputEmail(email, driver, wait);
        inputPassword(password, driver, wait);
        clickLogin(driver, wait);
    }
}
