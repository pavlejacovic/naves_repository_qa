package pageObjects;

import helpers.utils.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;

public class FunctionsPage {
     public static By navbar_myNavesFunctions_button = By.cssSelector("[data-test-id='myNavesFunctions']");
     public static By functions_createFunction_button = By.cssSelector("[data-test-id='createNewFunction']");
     public static By functions_dropDown_button = By.cssSelector("[data-test-id='dropdownBtn']");
     public static By functions_updateFuncExe_button = By.cssSelector("[data-test-id='updtNavesFunctionBtn']");
     public static By functions_deleteFunc_button = By.cssSelector("[data-test-id='deleteNavesFunctionBtn']");
     public static By functions_confirmDelete_button = By.cssSelector("[data-test-id='confirmBtn']");
     public static By functions_declineDelete_button = By.cssSelector("[data-test-id='declineBtn']");
     public static By functions_signOut_button = By.cssSelector("[data-test-id='signOutBtn']");
     public static By functions_createFuncBrowseFile_button = By.cssSelector("label[for='uploadJar']");
     public static By functions_saveNewFunc_button = By.cssSelector("[data-test-id='SaveNewFunction']");
     public static By functions_functionName_box = By.id("new-function");
     public static By functions_updateFuncProp_dropdown = By.cssSelector("[data-test-id='updateNavesFunctionBtn']");
     public static By functions_statistics_dropdown = By.cssSelector("[data-test-id='statisticNavesFunctionBtn']");
     public static By functions_statisticChart_class = By.className("recharts-surface");
     public static By functions_copyLink_button = By.cssSelector("button[class*='Button-Link']");
     public static By functions_selectLanguage_dropdown = By.cssSelector("input[id*='react-select']");
     public static By functions_browse_selectFuncExeFile = By.id("uploadJar");
     public static By functions_browse_updateFuncExeFile = By.id("uploadFile");
     public static By functions_getStatisticsForDays_box = By.id("days");
     public static By functions_getStatistic_button = By.cssSelector("[data-test-id='get-statistic']");
     public static By functions_upload_updateFuncExeFile_button = By.cssSelector("[data-test-id='sendChosenFilesBtn']");
     public static By functions_sharedWithMe_link = By.cssSelector("a[href*='shared']");
     public static By functions_myProjects_link = By.cssSelector("a[href*='projects']");
     public static By functions_toastSuccess_message = By.cssSelector("div[class*='success']");
     public static By functions_toastError_message = By.cssSelector("div[class*='error']");
     public static By functions_toastInfo_message = By.cssSelector("div[class*='info']");
     public static By functions_toastClose_button = By.cssSelector("button[aria-label='close']");
     public static By functions_inputFuncName_box = By.cssSelector("[data-test-id='creating-function-name-input-field']");
     public static By functions_error_message = By.cssSelector("p[class*='AlertMessage']");
     public static By functions_javaRadioBtn = By.id("java");
     public static By functions_CSharpRadioBtn = By.id("csharp");


     public static void inputFunctionName(WebDriver driver, WebDriverWait wait, String name){
          Actions.waitForElementToBeVisible(driver, wait, functions_functionName_box);
          driver.findElement(functions_functionName_box).clear();
          driver.findElement(functions_functionName_box).sendKeys(name);;
     }

     public static void selectDropdown(WebDriver driver, WebDriverWait wait, String selection, int i){
          driver.findElements(functions_selectLanguage_dropdown).get(i).sendKeys(selection);
          driver.findElements(functions_selectLanguage_dropdown).get(i).sendKeys(Keys.ENTER);
     }

     public static void clickCreateNewFunction(WebDriver driver, WebDriverWait wait){
          Actions.waitForElementToBeClickable(driver, wait, functions_createFunction_button);
          driver.findElement(functions_createFunction_button).click();
     }

     public static void clickSaveNewFunction(WebDriver driver, WebDriverWait wait){
          Actions.waitForElementToBeClickable(driver, wait, functions_saveNewFunc_button);
          driver.findElement(functions_saveNewFunc_button).click();
     }

     public static void inputExecutableFile(WebDriver driver, WebDriverWait wait, File file){
          driver.findElement(functions_browse_selectFuncExeFile).sendKeys(file.getAbsolutePath());
     }

     public static void createOrEditProperties(WebDriver driver, WebDriverWait wait, String name, String newName, String language, File file, String trigger, String type, String projectName){
          if(language != null){
               clickCreateNewFunction(driver, wait);
               inputFunctionName(driver, wait, name);
               selectDropdown(driver, wait, language, 0);
               if(file != null) inputExecutableFile(driver, wait, file);
               selectDropdown(driver, wait, trigger, 1);
               selectDropdown(driver, wait, type, 2);
               if(projectName != null) selectDropdown(driver, wait, projectName, 3);;
          }
          if(language == null){
               clickUpdateFunctionProp(driver, wait, name);
               inputFunctionName(driver, wait, newName);
               selectDropdown(driver, wait, trigger, 0);
               selectDropdown(driver, wait, type, 1);
               if(projectName != null) selectDropdown(driver, wait, projectName, 2);
          }
          clickSaveNewFunction(driver, wait);
     }

     public static By findFunctionElement(String name){
          return By.cssSelector("[data-test-id*='" + name);
     }

     public static void clickUpdateFunctionExe(WebDriver driver, WebDriverWait wait, String name){
          Actions.waitForElementToBeVisible(driver, wait , findFunctionElement(name));
          driver.findElement(findFunctionElement(name)).findElement(functions_dropDown_button).click();
          driver.findElement(functions_updateFuncExe_button).click();
     }

     public static void updateFunctionExeFile(WebDriver driver, WebDriverWait wait, File file){
          driver.findElement(functions_browse_updateFuncExeFile).sendKeys(file.getAbsolutePath());
     }

     public static void selectUpdExeLanguage(WebDriver driver, WebDriverWait wait, String language){
          wait.until(ExpectedConditions.presenceOfElementLocated(functions_javaRadioBtn));
          if (language.equalsIgnoreCase("java"))
               driver.findElement(functions_javaRadioBtn).click();
          if (language.equalsIgnoreCase("c#"))
               driver.findElement(functions_CSharpRadioBtn).click();
     }

     public static void clickUpdateFuncExe(WebDriver driver, WebDriverWait wait){
          driver.findElement(functions_upload_updateFuncExeFile_button).click();
     }

     public static void updateFuncExe(WebDriver driver, WebDriverWait wait, String name, String language, File file){
          clickUpdateFunctionExe(driver, wait, name);
          updateFunctionExeFile(driver, wait, file);
          selectUpdExeLanguage(driver, wait, language);
          clickUpdateFuncExe(driver, wait);
     }

     public static void clickDeleteFunc(WebDriver driver, WebDriverWait wait, String name){
          driver.findElement(findFunctionElement(name)).findElement(functions_dropDown_button).click();
          driver.findElement(functions_deleteFunc_button).click();
     }

     public static void confirmDelete(WebDriver driver, WebDriverWait wait){
          Actions.waitForElementToBeVisible(driver, wait, functions_confirmDelete_button);
          driver.findElement(functions_confirmDelete_button).click();
     }

     public static void deleteFunction(WebDriver driver, WebDriverWait wait, String name){
          clickDeleteFunc(driver, wait, name);
          confirmDelete(driver, wait);
     }

     public static void clickUpdateFunctionProp(WebDriver driver, WebDriverWait wait, String name){
          Actions.waitForElementToBeVisible(driver, wait , findFunctionElement(name));
          driver.findElement(findFunctionElement(name)).findElement(functions_dropDown_button).click();
          driver.findElement(functions_updateFuncProp_dropdown).click();
     }

     public static void clickStatisticsDropdown(WebDriver driver, WebDriverWait wait, String name){
          Actions.waitForElementToBeVisible(driver, wait , findFunctionElement(name));
          driver.findElement(findFunctionElement(name)).findElement(functions_dropDown_button).click();
          driver.findElement(functions_statistics_dropdown).click();
     }

     public static void inputDaysForStatistics(WebDriver driver, WebDriverWait wait, int days){
          Actions.waitForElementToBeVisible(driver, wait , functions_getStatisticsForDays_box);
          driver.findElement(functions_getStatisticsForDays_box).sendKeys(String.valueOf(days));
     }

     public static void clickToGetStatistics(WebDriver driver, WebDriverWait wait){
          Actions.waitForElementToBeVisible(driver, wait, functions_getStatistic_button);
          driver.findElement(functions_getStatistic_button).click();
     }

     public static void getStatistics(WebDriver driver, WebDriverWait wait, String name, int days){
          clickStatisticsDropdown(driver, wait, name);
          inputDaysForStatistics(driver, wait, days);
          clickToGetStatistics(driver, wait);
     }

     public static void clickCopyLink(WebDriver driver, WebDriverWait wait, String name) {
          driver.findElement(findFunctionElement(name)).findElement(functions_copyLink_button).click();
     }

     public static void openNewTabAndExecuteFunction(WebDriver driver, WebDriverWait wait, String name) throws AWTException {
          clickCopyLink(driver, wait, name);
          Robot r = new Robot();
          //opening new tab
          r.keyPress(KeyEvent.VK_CONTROL);
          r.keyPress(KeyEvent.VK_T);
          r.keyRelease(KeyEvent.VK_T);
          r.keyRelease(KeyEvent.VK_CONTROL);

          //copying link for executing function into new tab url
          r.keyPress(KeyEvent.VK_CONTROL);
          r.keyPress(KeyEvent.VK_V);
          r.keyRelease(KeyEvent.VK_V);
          r.keyRelease(KeyEvent.VK_CONTROL);

          r.keyPress(KeyEvent.VK_ENTER);
          r.keyRelease(KeyEvent.VK_ENTER);

     }


}
