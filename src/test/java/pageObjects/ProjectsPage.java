package pageObjects;

import static helpers.utils.Actions.*;

import helpers.utils.Actions;
import helpers.utils.Property;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

public class ProjectsPage {
    public static String url_welcome = Property.getProperty("mainPageUri");
    public static By projects_createNewProject_button = By.cssSelector("[data-test-id='createNewProject']");
    public static By projects_inputName_box = By.id("newProject");
    public static By projects_saveNewProject_button = By.cssSelector("[data-test-id='SaveNewProject']");
    public static By projects_blankName_message = By.cssSelector("p[class*='AlertMessage']");
    public static By projects_signOut_button = By.cssSelector("[data-test-id='signOutBtn']");
    public static By projects_dropdown_button = By.cssSelector("[data-test-id='dropdownBtn']");
    public static By projects_dropdownDownload_button = By.cssSelector("button[data-test-id='downloadBtn']");
    public static By projects_dropdownRename_button = By.cssSelector("button[data-test-id='renameBtn']");
    public static By projects_dropdownDelete_button = By.cssSelector("button[data-test-id='openConfirmDeleteModalBtn']");
    public static By projects_dropdownDeleteFolder_button = By.cssSelector("button[data-test-id='deleteBtn']");
    public static By projects_decline_button = By.cssSelector("button[data-test-id='declineBtn']");
    public static By projects_confirm_button = By.cssSelector("button[data-test-id='confirmBtn']");
    public static By projects_sharedWithMe_link = By.cssSelector("a[href*='shared']");
    public static By projects_myProjects_link = By.cssSelector("a[href*='projects']");
    public static By projects_createDirectory_button = By.cssSelector("button[data-test-id='createDirectoryBtn']");
    public static By projects_submit_button = By.cssSelector("button[type='submit']");
    public static By projects_upload_button = By.cssSelector("[data-test-id='opetModalForUploadBtn']");
    public static By projects_uploadBrowse_button = By.cssSelector("label[for='uploadFile']");
    public static By projects_uploadFile_input = By.id("uploadFile");
    public static By projects_uploadUpload_button = By.cssSelector("button[data-test-id='sendChosenFilesBtn']");
    public static By projects_renameFolder_button = By.cssSelector("button[data-test-id='renameFolderBtn']");
    public static By projects_renameProject_button = By.cssSelector("button[data-test-id='renameProjectBtn']");
    public static By projects_deleteFileOrFolder_button = By.cssSelector("button[data-test-id='deleteBtn']");
    public static By projects_toastSuccess_message = By.cssSelector("div[class*='success']");
    public static By projects_toastError_message = By.cssSelector("div[class*='error']");
    public static By getProjects_toastWarning_message = By.cssSelector("div[class*='warning']");
    public static By projects_toastInfo_message = By.cssSelector("div[class*='info']");
    public static By projects_toastClose_button = By.cssSelector("button[aria-label='close']");

    public static By projects_projectsClass = By.cssSelector("a[class*='ProjectFolder']");
    public static By projects_sortBy_button = By.id("sortProjects");
    public static By projects_newToOld_button = By.name("newest to oldest");
    public static By projects_oldToNew_button = By.name("oldest to newest");
    public static By projects_nameAsc_button = By.name("name ascending");
    public static By projects_nameDesc_button = By.name("name descending");
    public static By projects_sizeAsc_button = By.name("size increasing");
    public static By projects_sizeDesc_button = By.name("size decreasing");
    public static By projects_perPage_button = By.id("projectsPerPage");
    public static String projects_12perPage_button = "12";
    public static String projects_24perPage_button = "24";
    public static String projects_48perPage_button = "48";
    public static String newToOld = "id,desc";
    public static String oldToNew = "id";
    public static String nameAsc = "projectName";
    public static String nameDesc = "projectName,desc";
    public static String sizeDesc = "projectSize,desc";
    public static String sizeAsc = "projectSize";

    public static void sortBy(WebDriver driver, String id){
        Select se = new Select(driver.findElement(projects_sortBy_button));
        se.selectByValue(id);
    }

    public static void projectsPerPage(WebDriver driver, String number){
        Select se = new Select(driver.findElement(projects_perPage_button));
        se.selectByValue(number);
    }

    public static By findProjectElement(String name){
        return By.cssSelector("[data-test-id*='" + name + "']:not([type='checkbox'])");
    }
    public static void downloadProjectElement(String name, WebDriver driver){
        driver.findElement(findProjectElement(name)).findElement(projects_dropdown_button).click();
        driver.findElement(projects_dropdownDownload_button).click();
    }

    public static void renameProjectElement(WebDriver driver, WebDriverWait wait, String name, String newName){
        driver.findElement(findProjectElement(name)).findElement(projects_dropdown_button).sendKeys(Keys.ENTER);
        driver.findElement(projects_dropdownRename_button).sendKeys(Keys.ENTER);
        Actions.sendTextToElement(driver, wait, projects_inputName_box, newName);
    }

    public static void deleteProjectElement(String name, WebDriverWait wait, WebDriver driver){
        driver.findElement(findProjectElement(name)).findElement(projects_dropdown_button).click();
        waitForElementToBeVisible(driver, wait, projects_dropdownDelete_button);
        driver.findElement(projects_dropdownDelete_button).click();
        driver.findElement(projects_confirm_button).click();
    }

    public static void deleteFolder(String name, WebDriverWait wait, WebDriver driver){
        driver.findElement(findProjectElement(name)).findElement(projects_dropdown_button).click();
        waitForElementToBeVisible(driver, wait, projects_dropdownDeleteFolder_button);
        driver.findElement(projects_dropdownDeleteFolder_button).click();
        driver.findElement(projects_confirm_button).click();
    }

    public static void createNewProject(WebDriver driver, WebDriverWait wait, String name){
        waitForElementToBeVisible(driver, wait, projects_createNewProject_button);
        driver.findElement(projects_createNewProject_button).click();
        waitForElementToBeVisible(driver, wait, projects_inputName_box);
        sendTextToElement(driver, wait, projects_inputName_box, name);
        waitForElementToBeVisible(driver, wait, projects_saveNewProject_button);
        driver.findElement(projects_saveNewProject_button).click();
    }

    public static void createNewFolder(WebDriver driver, WebDriverWait wait, String name){
        waitForElementToBeVisible(driver, wait, projects_createDirectory_button);
        driver.findElement(projects_createDirectory_button).click();
        sendTextToElement(driver, wait, projects_inputName_box, name);
        waitForElementToBeVisible(driver, wait, projects_submit_button);
        driver.findElement(projects_submit_button).click();
    }

    public static void openProjectOrFolder(WebDriver driver, WebDriverWait wait, String name){
        waitForElementToBeVisible(driver, wait, findProjectElement(name));
        driver.findElement(findProjectElement(name)).click();
    }

    public static void renameFolder(WebDriver driver, WebDriverWait wait, String oldName, String newName){
        renameProjectElement(driver, wait, oldName, newName);
        driver.findElement(projects_renameFolder_button).click();
    }

    public static void renameProject(WebDriver driver, WebDriverWait wait, String oldName, String newName){
        renameProjectElement(driver, wait, oldName, newName);
        driver.findElement(projects_renameProject_button).click();
    }

    public static void renameFile(WebDriver driver, WebDriverWait wait, File file, String newName){
        renameProjectElement(driver, wait, file.getName(), newName);
        driver.findElement(projects_renameFolder_button).click();
    }

    public static void downloadProjectOrFolder(WebDriver driver, WebDriverWait wait, String name){
        waitForElementToBeVisible(driver, wait, projects_dropdown_button);
        driver.findElement(findProjectElement(name)).findElement(projects_dropdown_button).click();
        driver.findElement(projects_dropdownDownload_button).click();
        Alert alert = new Alert() {
            @Override
            public void dismiss() {
            }
            @Override
            public void accept() {
            }
            @Override
            public String getText() {
                return null;
            }
            @Override
            public void sendKeys(String s) {
            }
        };
        alert.accept();
    }

    public static void uploadFiles(WebDriver driver, File[] files){
        driver.findElement(projects_upload_button).click();
        StringBuilder sb = new StringBuilder();
        for (File file : files)
            driver.findElement(projects_uploadFile_input).sendKeys(file.getAbsolutePath());
        driver.findElement(projects_uploadUpload_button).click();
    }
}