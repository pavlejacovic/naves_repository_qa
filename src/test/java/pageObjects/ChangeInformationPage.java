package pageObjects;

import helpers.utils.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChangeInformationPage {

    public static By userInfo_signOut_button = By.cssSelector("[data-test-id='signOutBtn']");
    public static By userInfo_sharedWithMe_link = By.cssSelector("a[href*='shared']");
    public static By userInfo_myProjects_link = By.cssSelector("a[href*='projects']");
    public static By userInfo_toastSuccess_message = By.cssSelector("div[class*='success']");
    public static By userInfo_toastError_message = By.cssSelector("div[class*='error']");
    public static By userInfo_toastWarning_message = By.cssSelector("div[class*='warning']");
    public static By userInfo_toastInfo_message = By.cssSelector("div[class*='info']");
    public static By userInfo_toastClose_button = By.cssSelector("button[aria-label='close']");
    public static By userInfo_changePassword_button = By.xpath("//*[text()='Change Password']");
    public static By userInfo_editInformation_button = By.cssSelector("[title='Edit']");
    public static By userInfo_name_box = By.id("name");
    public static By userInfo_surname_box = By.id("surname");
    public static By userInfo_subscribed_button = By.cssSelector("input[data-test-id='subscribed-yes']");
    public static By userInfo_submit_button = By.cssSelector("button[type='submit']");
    public static By userInfo_cancel_button = By.xpath("//*[@id=\"root\"]/div/div[1]/div[2]/div[2]/div[4]/div/div/div[1]/div/div/div/button"); //has to be this way
    public static By userInfo_oldPass_box = By.id("old-password");
    public static By userInfo_newPass_box = By.id("new-password");
    public static By userInfo_confirmPass_box = By.id("confirmed-password");
    public static By resetPassword_unsuccessfulChange_message = By.cssSelector("p[class*='AlertMessage_AlertMessage_warning']");

    public static void clickOnSubmit(WebDriver driver, WebDriverWait wait){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_submit_button);
        driver.findElement(userInfo_submit_button).click();
    }

    public static void clickOnEditInfo(WebDriver driver, WebDriverWait wait){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_editInformation_button);
        driver.findElement(userInfo_editInformation_button).click();
    }

    public static void inputName(WebDriver driver, WebDriverWait wait, String name){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_name_box);
        Actions.sendTextToElement(driver, wait, userInfo_name_box, name);
    }

    public static void inputSurname(WebDriver driver, WebDriverWait wait, String surname){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_surname_box);
        Actions.sendTextToElement(driver, wait, userInfo_surname_box, surname);
    }

    public static void clickOnSubscribed(WebDriver driver, WebDriverWait wait){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_subscribed_button);
        driver.findElement(userInfo_subscribed_button).click();
    }

    public static void changeUserInfo(WebDriver driver, WebDriverWait wait, String name, String surname){
        clickOnEditInfo(driver, wait);
        inputName(driver, wait, name);
        inputSurname(driver, wait, surname);
        clickOnSubscribed(driver, wait);
        driver.findElement(userInfo_submit_button).click();
    }

    public static void clickOnEditPassword(WebDriver driver, WebDriverWait wait){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_changePassword_button);
        driver.findElement(userInfo_changePassword_button).click();
    }

    public static void inputOldPassword(WebDriver driver, WebDriverWait wait, String oldPass){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_oldPass_box);
        Actions.sendTextToElement(driver, wait, userInfo_oldPass_box, oldPass);
    }

    public static void inputNewPassword(WebDriver driver, WebDriverWait wait, String newPass){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_newPass_box);
        Actions.sendTextToElement(driver, wait, userInfo_newPass_box, newPass);
    }

    public static void inputConfirmPassword(WebDriver driver, WebDriverWait wait, String confirmPass){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_confirmPass_box);
        Actions.sendTextToElement(driver, wait, userInfo_confirmPass_box, confirmPass);
    }

    public static void changeUserPassword(WebDriver driver, WebDriverWait wait, String oldPass, String newPass, String confirmPass){
        inputOldPassword(driver, wait, oldPass);
        inputNewPassword(driver, wait, newPass);
        inputConfirmPassword(driver, wait, confirmPass);
        driver.findElement(userInfo_submit_button).click();
    }

    public static void clearPasswordField(WebDriver driver, WebDriverWait wait){
        Actions.waitForElementToBeVisible(driver, wait, userInfo_editInformation_button);
        clickOnEditPassword(driver, wait);
        Actions.waitForElementToBeVisible(driver, wait, userInfo_oldPass_box);
        driver.findElement(userInfo_oldPass_box).clear();
        driver.findElement(userInfo_newPass_box).clear();
        driver.findElement(userInfo_confirmPass_box).clear();
    }

    public static void clickCancel(WebDriver driver, WebDriverWait wait) {
        Actions.waitForElementToBeClickable(driver, wait, userInfo_cancel_button);
        driver.findElement(userInfo_cancel_button).click();
    }
}
