package rest.user;

import helpers.Constants;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.pojo.user.ChangeUserInformationRequestPojo;
import helpers.pojo.user.GetUserInformationResponsePojo;
import helpers.requests.Authentication;
import helpers.requests.User;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import io.restassured.response.Response;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

public class UserChangeInformation extends BaseRest {

    ChangeUserInformationRequestPojo requestPojo = new ChangeUserInformationRequestPojo();
    GetUserInformationResponsePojo responsePojo = new GetUserInformationResponsePojo();
    Response response;
    String changeName;
    String changeSurname;
    String changeSubscribed;

    @BeforeMethod
    public void prepareChangeData(){
        changeName = GenerateRandom.randomValidName();
        changeSurname = GenerateRandom.randomValidSurname();
        changeSubscribed = "false";
        requestPojo.setName(changeName);
        requestPojo.setSurname(changeSurname);
        requestPojo.setSubscribed(changeSubscribed);
    }

    @Test
    public void shouldChangeUserInformation(){
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 200);
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), changeName);
        sa.assertEquals(responsePojo.getSurname(), changeSurname);
        sa.assertEquals(responsePojo.getSubscribed().toString(), changeSubscribed);
        sa.assertAll();
    }

    @Test
    public void shouldChangeUserInformationWithoutSubscribedField(){
        requestPojo.setSubscribed(null);
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 200);
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), changeName);
        sa.assertEquals(responsePojo.getSurname(), changeSurname);
        sa.assertEquals(responsePojo.getSubscribed().toString(), "false");
        sa.assertAll();
    }

    @Test
    public void shouldNotChangeUserInformationInvalidSubscribedField(){
        requestPojo.setSubscribed("randomString");
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangeUserInformationBlankName(){
        requestPojo.setName("");
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 400);
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), testUser.get("name").getAsString());
        sa.assertEquals(responsePojo.getSurname(), testUser.get("surname").getAsString());
        sa.assertEquals(responsePojo.getSubscribed().toString(), testUser.get("subscribed").getAsString());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangeUserInformationBlankSurname(){
        requestPojo.setSurname("");
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 400);
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), testUser.get("name").getAsString());
        sa.assertEquals(responsePojo.getSurname(), testUser.get("surname").getAsString());
        sa.assertEquals(responsePojo.getSubscribed().toString(), testUser.get("subscribed").getAsString());
        sa.assertAll();
    }


    @Test
    public void shouldNotChangeUserInformationLongName(){
        requestPojo.setName(Constants.invalidNameTooLong);
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 400);
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), testUser.get("name").getAsString());
        sa.assertEquals(responsePojo.getSurname(), testUser.get("surname").getAsString());
        sa.assertEquals(responsePojo.getSubscribed().toString(), testUser.get("subscribed").getAsString());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangeUserInformationLongSurname(){
        requestPojo.setSurname(Constants.invalidNameTooLong);
        sa.assertEquals(User.changeUserInformation(token, requestPojo).statusCode(), 400);
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), testUser.get("name").getAsString());
        sa.assertEquals(responsePojo.getSurname(), testUser.get("surname").getAsString());
        sa.assertEquals(responsePojo.getSubscribed().toString(), testUser.get("subscribed").getAsString());
        sa.assertAll();
    }

    @Test
    public void shouldNotChangeUserInformationNoToken(){
        sa.assertEquals(User.changeUserInformation("", requestPojo).statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangeUserInformationInvalidToken(){
        sa.assertEquals(User.changeUserInformation("randomTokenString", requestPojo).statusCode(), 401);
        sa.assertAll();
    }

    @AfterMethod
    public void resetUserData(){
        User.resetUser(token, testUser);
    }
}