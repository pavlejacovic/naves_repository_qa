package rest.user;

import helpers.generators.UserBuilder;
import helpers.pojo.function.ListFunctionsResponsePojo;
import helpers.pojo.user.GetUserInformationResponsePojo;
import helpers.requests.User;
import io.restassured.response.Response;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

public class UserGetInformation extends BaseRest {

    Response response;
    GetUserInformationResponsePojo responsePojo;

    @BeforeMethod
    public void prepareForTests(){
        sa = new SoftAssert();
    }

    @Test
    public void shouldGetUserInformation(){
        response = User.getUserInformation(token);
        responsePojo = response.getBody().as(GetUserInformationResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getEmail(), testUser.get("email").getAsString());
        sa.assertEquals(responsePojo.getName(), testUser.get("name").getAsString());
        sa.assertEquals(responsePojo.getSurname(), testUser.get("surname").getAsString());
        sa.assertEquals(responsePojo.getSubscribed().toString(), testUser.get("subscribed").getAsString());
        sa.assertAll();
    }

    @Test
    public void shouldNotGetUserInformationInvalidToken(){
        response = User.getUserInformation("randomTokenString");
        sa.assertEquals(response.statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetUserInformationNoToken(){
        response = User.getUserInformation("");
        sa.assertEquals(response.statusCode(), 401);
        sa.assertAll();
    }
}