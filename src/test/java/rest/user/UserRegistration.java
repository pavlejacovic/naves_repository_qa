//Sasa Avramovic
package rest.user;

import helpers.Constants;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.sql.SQLException;

import static io.restassured.RestAssured.given;

public class UserRegistration extends BaseRest {

    @BeforeMethod
    public void prepareUserBody(){
        sa = new SoftAssert();
        testUser = UserBuilder.testUser();
    }

    //test registration with valid credentials, no validation, expected 200
    @Test
    public void shouldRegisterNewUserWithoutValidation(){
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 200);
        sa.assertAll();
    }

    //test registration with valid credentials and validation, expected 200
    @Test
    public void shouldRegisterNewUserWithValidation() {
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 200);
        testUser.addProperty("token", DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "confirmation_token"));
        sa.assertEquals(Authentication.validateUser(testUser).statusCode(), 200);
        sa.assertAll();
    }

    //test registration without email, expected error 400
    @Test
    public void shouldNotRegisterNewUserWithoutEmail(){
        testUser.addProperty("email", "");
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration without name, expected 400
    @Test
    public void shouldNotRegisterNewUserWithoutName(){
        testUser.addProperty("name", "");
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration without surname, expected 400
    @Test
    public void shouldNotRegisterNewUserWithoutSurname(){
        testUser.addProperty("surname", "");
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration without password, expected 400
    @Test
    public void shouldNotRegisterNewUserWithoutPassword(){
        testUser.addProperty("password", "");
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with invalid password, no numbers, expected 400
    @Test
    public void shouldNotRegisterNewUserPasswordNoNumbers(){
        testUser.addProperty("password", Constants.invalidPasswordNoNumber);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with invalid password, too short, expected 400
    @Test
    public void shouldNotRegisterNewUserPasswordSevenCharacters(){
        testUser.addProperty("password", Constants.invalidPasswordSevenChars);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with invalid password, no capital letters, expected 400
    @Test
    public void shouldNotRegisterNewUserPasswordNoCapitalLetter(){
        testUser.addProperty("password", Constants.invalidPasswordNoCapitalLetter);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with invalid password, all capital letters, expected 400
    @Test
    public void shouldNotRegisterNewUserPasswordAllCaps(){
        testUser.addProperty("password", Constants.invalidPasswordAllCaps);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with invalid password, no special characters, expected 400
    @Test
    public void shouldNotRegisterNewUserPasswordNoSpecial(){
        testUser.addProperty("password",Constants.invalidPasswordNoSpecial);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with invalid password, password too long, expected 400
    @Test
    public void shouldNotRegisterNewUserLongPassword(){
        testUser.addProperty("password", Constants.invalidPasswordTooLong);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with valid credentials, valid password 255 characters, expected 200
    @Test
    public void shouldRegisterNewUserMaxPassword(){
        testUser.addProperty("password", Constants.validPasswordMaxChars);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 200);
        sa.assertAll();
    }

    //test registration with invalid mail format, expected 400
    @Test
    public void shouldNotRegisterNewUserInvalidEmailFormat(){
        testUser.addProperty("email", GenerateRandom.randomValidName());
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with name too long, expected 400
    @Test
    public void shouldNotRegisterNewUserNameTooLong(){
        testUser.addProperty("name", Constants.invalidNameTooLong);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with surname too long, expected 400
    @Test
    public void shouldNotRegisterNewUserSurnameTooLong(){
        testUser.addProperty("name", Constants.invalidSurnameTooLong);
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 400);
        sa.assertAll();
    }

    //test registration with subscription
    @Test
    public void shouldRegisterWithSubscription() {
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 200);
        testUser.addProperty("token", DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "confirmation_token"));
        sa.assertEquals(Authentication.validateUser(testUser).statusCode(), 200);
        //check subscription status
        sa.assertEquals(DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "subscribed"), "1");
        sa.assertAll();
    }

    //test registration with subscription
    @Test
    public void shouldRegisterWithoutSubscription() {
        testUser.addProperty("subscribed", "false");
        sa.assertEquals(Authentication.registerUser(testUser).statusCode(), 200);
        testUser.addProperty("token", DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "confirmation_token"));
        sa.assertEquals(Authentication.validateUser(testUser).statusCode(), 200);
        sa.assertEquals(DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser), "subscribed"), "0");
        sa.assertAll();
    }

    @AfterMethod
    public void deleteUserFromDatabase(){
        DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(testUser));
    }
}