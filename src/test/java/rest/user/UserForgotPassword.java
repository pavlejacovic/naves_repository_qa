package rest.user;

import helpers.requests.Authentication;
import helpers.utils.GetUserPropertyFromJson;
import org.testng.annotations.Test;
import rest.BaseRest;

public class UserForgotPassword extends BaseRest {

    @Test
    public void shouldSendResetPasswordEmail(){
        sa.assertEquals(Authentication.sendResetPasswordEmail(email).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void invalidSendEmailInvalidEmail(){
        testUser.addProperty("email", "invalid@email/com");
        sa.assertEquals(Authentication.sendResetPasswordEmail(GetUserPropertyFromJson.getEmail(testUser)).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void invalidSendEmailInvalidEmail2(){
        testUser.addProperty("email", "invalidEmail");
        sa.assertEquals(Authentication.sendResetPasswordEmail(GetUserPropertyFromJson.getEmail(testUser)).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void invalidSendEmailEmptyEmail(){
        testUser.addProperty("email", "");
        sa.assertEquals(Authentication.sendResetPasswordEmail(GetUserPropertyFromJson.getEmail(testUser)).statusCode(), 400);
        sa.assertAll();
    }
}
