package rest.user;

import helpers.Constants;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.UserBuilder;
import com.google.gson.JsonObject;
import helpers.requests.Authentication;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import org.testng.Assert;
import org.testng.annotations.*;
import rest.BaseRest;

import static io.restassured.RestAssured.given;

public class UserResetPassword extends BaseRest {

    @BeforeMethod
    public void sendResetPasswordEmail() {
        Authentication.sendResetPasswordEmail(email);
        passwordResetToken = DataBaseManipulation.getFromDatabase(email, "reset_password_token");
        resetPasswordBody.addProperty("password", password);
        resetPasswordBody.addProperty("passwordResetToken", passwordResetToken);
    }

    //Finds passwordResetToken and posts it along with new password to auth/reset-password
    @Test
    public void shouldResetPasswordWithMinimumCharacters() {
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 200);
    }

    //Testing resetting of password with maximum characters
    @Test
    public void shouldResetPassword255MaxChars() {
        resetPasswordBody.addProperty("password", Constants.validPasswordMaxChars);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 200);

    }

    //Testing resetting of password without token
    @Test
    public void shouldNotResetPasswordWithoutToken() {
        resetPasswordBody.addProperty("passwordResetToken", "");
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password without new password
    @Test
    public void shouldNotResetPasswordWithoutNewPassword() {
        resetPasswordBody.addProperty("password", "");
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password without minimum chars
    @Test
    public void shouldNotResetPasswordWithoutMinimumCharacters() {
        resetPasswordBody.addProperty("password", Constants.invalidPasswordSevenChars);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password exceeding maximum chars
    @Test
    public void shouldNotResetPasswordExceedingMaximumCharacters() {
        resetPasswordBody.addProperty("password", Constants.invalidPasswordTooLong);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password without capital letter
    @Test
    public void shouldNotResetPasswordWithoutCapitalLetter() {
        resetPasswordBody.addProperty("password", Constants.invalidPasswordNoCapitalLetter);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password without number
    @Test
    public void shouldNotResetPasswordWithoutNumber() {
        resetPasswordBody.addProperty("password", Constants.invalidPasswordNoNumber);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password without small letter
    @Test
    public void shouldNotResetPasswordWithoutSmallLetters() {
        resetPasswordBody.addProperty("password", Constants.invalidPasswordAllCaps);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password without special character
    @Test
    public void shouldNotResetPasswordWithoutSpecialCharacters() {
        resetPasswordBody.addProperty("password", Constants.invalidPasswordNoSpecial);
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody).getStatusCode(), 400);
    }

    //Testing resetting of password with a new unverified user
    @Test
    public void shouldNotResetPasswordWithoutVerifyingUser() {
        JsonObject testUser2;
        JsonObject resetPasswordBody2 = new JsonObject();
        String email2;
        testUser2 = UserBuilder.testUser();
        Authentication.registerUser(testUser2);
        email2 = GetUserPropertyFromJson.getEmail(testUser2);
        Authentication.sendResetPasswordEmail(email2);
        resetPasswordBody2.addProperty("password", Constants.validPasswordEightChars);
        String token2 = DataBaseManipulation.getFromDatabase(GetUserPropertyFromJson.getEmail(testUser2), "reset_password_token");
        resetPasswordBody2.addProperty("passwordResetToken", "invalidToken");
        Assert.assertEquals(Authentication.resetPassword(resetPasswordBody2).getStatusCode(), 401);
        DataBaseManipulation.deleteUser(email2);
    }
}