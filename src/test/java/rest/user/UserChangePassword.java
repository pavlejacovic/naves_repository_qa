package rest.user;

import helpers.Constants;
import helpers.generators.GenerateRandom;
import helpers.pojo.user.ChangePasswordRequestPojo;
import helpers.pojo.user.ChangeUserInformationRequestPojo;
import helpers.requests.User;
import helpers.utils.Property;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

public class UserChangePassword extends BaseRest {

    String changePassword;
    ChangePasswordRequestPojo requestPojo = new ChangePasswordRequestPojo();

    @BeforeMethod
    public void prepareTestData(){
        changePassword = GenerateRandom.randomValidPassword();
        requestPojo.setOldPassword(testUser.get("password").getAsString());
        requestPojo.setNewPassword(changePassword);
    }

    @Test
    public void shouldChangePassword(){
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidToken(){
        sa.assertEquals(User.changePassword("randomString", requestPojo).statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordNoToken(){
        sa.assertEquals(User.changePassword("", requestPojo).statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordInvalidOldPassword(){
        requestPojo.setOldPassword("randomString");
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 409);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordBlankOldPassword(){
        requestPojo.setOldPassword("");
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordNullOldPassword(){
        requestPojo.setOldPassword(null);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordBlankNewPassword(){
        requestPojo.setNewPassword("");
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordNullNewPassword(){
        requestPojo.setNewPassword(null);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordSameNewPassword(){
        requestPojo.setNewPassword(testUser.get("password").getAsString());
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 409);
        sa.assertAll();
    }

    @Test
    public void shouldNotChangePasswordShortInvalidPassword(){
        requestPojo.setNewPassword(Constants.invalidPasswordSevenChars);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        requestPojo.setNewPassword(Constants.invalidPasswordNoCapitalLetter);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        requestPojo.setNewPassword(Constants.invalidPasswordNoNumber);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        requestPojo.setNewPassword(Constants.invalidPasswordNoSpecial);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        requestPojo.setNewPassword(Constants.invalidPasswordAllCaps);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        requestPojo.setNewPassword(Constants.invalidPasswordTooLong);
        sa.assertEquals(User.changePassword(token, requestPojo).statusCode(), 400);
        sa.assertAll();
    }

    @AfterMethod
    public void resetPassword(){
        User.resetPassword(token, changePassword, testUser);
    }
}
