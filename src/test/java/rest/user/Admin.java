package rest.user;

import com.google.gson.JsonObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.pojo.project.ShareProjectResponsePojo;
import helpers.pojo.user.AdminGetProjectsResponsePojo;
import helpers.requests.*;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.BaseRest;
import selenium.ui.functions.BoxFunctions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Admin extends BaseRest {

    int numberOfNewUsers = 5;
    int numberOfProjectsPerUser = 2;
    int numberOfFunctionsPerUser = 1;
    int numberOfFilesToUpload = 2;
    String admin = Property.getProperty("admin");
    JsonObject[] tempUsers = new JsonObject[numberOfNewUsers * numberOfProjectsPerUser];
    File[] testFiles;
    String[] tokens = new String[numberOfNewUsers];
    String[] emails = new String[numberOfNewUsers];
    String[] randomProjectNames = new String[numberOfNewUsers * numberOfProjectsPerUser];
    String[] randomFunctionNames = new String[numberOfNewUsers * numberOfFunctionsPerUser];
    ArrayList<Boolean> check = new ArrayList<>();
    Response response;
    int startingNumberOfUsers;
    int startingNumberOfProjects;
    int startingNumberOfFunctions;
    AdminGetProjectsResponsePojo[] responsePojo;
    File file;
    String trigger;
    boolean publicAccess;
    Integer[] prID = new Integer[1];

    @BeforeClass
    public void prepareDataForTests() throws IOException {
        file = BoxFile.createRandomFile(".jar");
        file.deleteOnExit();
        trigger = "GET";
        publicAccess = true;
        prID[0] = null;
        startingNumberOfUsers = Integer.parseInt(AdminRequests.getNumberOfClients(AdminRequests.getAdminToken()).body().asString());
        startingNumberOfProjects = Integer.parseInt(AdminRequests.getNumberOfProjects(AdminRequests.getAdminToken()).body().asString());
        startingNumberOfFunctions = Integer.parseInt(AdminRequests.getNumberOfFunctions(AdminRequests.getAdminToken()).body().asString());
        testFiles = BoxFile.createRandomFiles(numberOfFilesToUpload);
        for (int i = 0; i < numberOfNewUsers; i++) {
            tempUsers[i] = UserBuilder.testUser();
            tokens[i] = Authentication.registerLoginReturnToken(tempUsers[i]);
            emails[i] = GetUserPropertyFromJson.getEmail(tempUsers[i]);
            for (int j = (i*numberOfFunctionsPerUser); j < ((i+1)*numberOfFunctionsPerUser); j++){
                randomFunctionNames[j] = GenerateRandom.randomDataName();
                NavesFunction.createNavesFunction(tokens[i], randomFunctionNames[i], publicAccess, trigger, file, language, null);
            }
            for (int j = (i*numberOfProjectsPerUser); j < ((i+1)*numberOfProjectsPerUser); j++){
                randomProjectNames[j] = GenerateRandom.randomDataName();
                projectBody.addProperty("projectName", randomProjectNames[j]);
                BoxProject.createProject(projectBody, tokens[i]);
                BoxFile.uploadFiles(testFiles, "", DataBaseManipulation.getProjectID(emails[i], randomProjectNames[j]), tokens[i], check).statusCode();
            }
        }
    }

    @Test
    public void shouldGetNumberOfUsers(){
        response = AdminRequests.getNumberOfClients(AdminRequests.getAdminToken());
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals((Integer.parseInt(AdminRequests.getNumberOfClients(AdminRequests.getAdminToken()).body().asString())), (startingNumberOfUsers + numberOfNewUsers));
        sa.assertAll();
    }

    @Test
    public void shouldGetNumberOfProjects(){
        response = AdminRequests.getNumberOfClients(AdminRequests.getAdminToken());
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals((Integer.parseInt(AdminRequests.getNumberOfProjects(AdminRequests.getAdminToken()).body().asString())), (startingNumberOfProjects + (numberOfNewUsers * numberOfProjectsPerUser)));
        sa.assertAll();
    }

    @Test
    public void shouldGetProjectsWithDetails(){
        response = AdminRequests.getProjectsWithDetails(AdminRequests.getAdminToken());
        responsePojo = response.getBody().as(AdminGetProjectsResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        for (int i = startingNumberOfProjects; i < responsePojo.length; i+=2){
            sa.assertEquals(responsePojo[i].getProject().getOwner().getEmail(), emails[(i - startingNumberOfProjects)/2]);
        }
        sa.assertAll();
    }

    @Test
    public void shouldGetProjectsWithPagination(){
        response = AdminRequests.getProjectsWithPagination(AdminRequests.getAdminToken(), 2, 2);
        responsePojo = response.getBody().as(AdminGetProjectsResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.length, 2);
        sa.assertAll();
    }

    @Test
    public void shouldGetNumberOfFunctions(){
        response = AdminRequests.getNumberOfFunctions(AdminRequests.getAdminToken());
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals((Integer.parseInt(AdminRequests.getNumberOfFunctions(AdminRequests.getAdminToken()).body().asString())), (startingNumberOfFunctions + (numberOfNewUsers * numberOfFunctionsPerUser)));
        sa.assertAll();
    }

    @Test
    public void shouldNotGetNumberOfUsersInvalidToken(){
        sa.assertEquals(AdminRequests.getNumberOfClients(token).statusCode(), 403);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetNumberOfUsersNoToken(){
        sa.assertEquals(AdminRequests.getNumberOfClients("").statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetNumberOfProjectsInvalidToken(){
        sa.assertEquals(AdminRequests.getNumberOfClients(token).statusCode(), 403);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetNumberOfProjectsNoToken(){
        sa.assertEquals(AdminRequests.getNumberOfClients("").statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetProjectsWithDetailsInvalidToken(){
        sa.assertEquals(AdminRequests.getProjectsWithDetails(token).statusCode(), 403);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetProjectsWithDetailsNoToken(){
        sa.assertEquals(AdminRequests.getProjectsWithDetails("").statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetProjectsWithPaginationInvalidToken(){
        sa.assertEquals(AdminRequests.getProjectsWithPagination(token, 2, 2).statusCode(), 403);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetProjectsWithPaginationNoToken(){
        sa.assertEquals(AdminRequests.getProjectsWithPagination("", 2, 2).statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetNumberOfFunctionsInvalidToken(){
        sa.assertEquals(AdminRequests.getNumberOfFunctions(token).statusCode(), 403);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetNumberOfFunctionsNoToken(){
        sa.assertEquals(AdminRequests.getNumberOfFunctions("").statusCode(), 401);
        sa.assertAll();
    }

    @AfterClass
    public void deleteDataAfterTests(){
        for (int i = 0; i < numberOfNewUsers; i++) {
            for (int j = (i*numberOfProjectsPerUser); j < ((i+1)*numberOfProjectsPerUser); j++){
                NavesFunction.deleteNavesFunction(tokens[i], DataBaseManipulation.getNavesFunctionID(emails[i], randomFunctionNames[i]));
                BoxProject.deleteProject(tokens[i], DataBaseManipulation.getProjectID(emails[i], randomProjectNames[j]));
            }
            DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(tempUsers[i]));
        }
    }
}