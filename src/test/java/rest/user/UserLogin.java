package rest.user;
//Boris_Nedeljkovic

import helpers.Constants;
import helpers.ReadProperties;
import helpers.requests.Authentication;
import helpers.utils.Property;
import org.aeonbits.owner.ConfigFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import rest.BaseRest;

import static io.restassured.RestAssured.given;

public class UserLogin extends BaseRest {

    //Test should use email and password made in builder and registered and verified in beforeClass to login
    @Test
    public void shouldLogin() {
        Assert.assertEquals(Authentication.logInUser(testUser).getStatusCode(), 200);
    }

    //User should only be able to login using his own password
    @Test
    public void shouldNotLoginWithInvalidPass() {
        testUser.addProperty("password", Constants.validPasswordNineChars);
        Assert.assertEquals(Authentication.logInUser(testUser).getStatusCode(), 403);
    }

    //User should not be able to login with empty password
    @Test
    public void shouldNotLoginWithEmptyPass() {
        testUser.addProperty("password", "");
        Assert.assertEquals(Authentication.logInUser(testUser).getStatusCode(), 403);
    }

    //User should not be able to login with empty email
    @Test
    public void shouldNotLoginWithEmptyEmail() {
        testUser.addProperty("password", Constants.validPasswordEightChars);
        testUser.addProperty("email", "");
        Assert.assertEquals(Authentication.logInUser(testUser).getStatusCode(), 403);
    }

    //User should not be able to login using invalid email
    @Test
    public void shouldNotLoginWithInvalidEmail() {
        testUser.addProperty("password", Constants.validPasswordEightChars);
        testUser.addProperty("email", Constants.invalidEmailDotFirst);
        Assert.assertEquals(Authentication.logInUser(testUser).getStatusCode(), 403);
    }
}