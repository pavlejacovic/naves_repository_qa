package rest.box.file;

import helpers.requests.BoxProject;
import helpers.utils.UtilsForAPI;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxFolder;
import org.testng.annotations.*;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;


import static io.restassured.RestAssured.given;

//folder is another directory inside project directory
//localhost:9191/box/upload/{proj_id}
//for loop pulls status from response body and asserts that upload was successful
public class FileUpload extends BaseRest {
    File[] files;
    ArrayList<Boolean> checkUpload = new ArrayList<>();
    ArrayList<Boolean> checkDelete = new ArrayList<>();
    @BeforeMethod
    public void createProject(){
        BoxProject.createProject(projectBody, token);
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        randomFolderName = GenerateRandom.randomDataName();
    }

    //making 1 temporary file, making 1 temp project and 1 folder, uploading file to folder, deleting them all after
    @Test
    public void shouldUploadOneFileToFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload ).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertAll();
    }

    //making 10 (maximum upload for now) temporary files, uploading them locally, deleting them after
    @Test
    public void shouldUploadTenFilesToFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(10);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 10);
        sa.assertAll();
    }
/*
    //making 10 (maximum upload for now) temporary files, 5 of them invalid name, uploading them locally, deleting them after
    @Test
    public void shouldUploadFiveOfTenFilesToFolder() throws IOException, SQLException {
        randomFolderName = GenerateRandom.randomDataName();
        files = BoxFile.createRandomFiles(10);
        File[] renameFile = new File[files.length];
        for (int i = 0; i < files.length; i ++) {
            if ((i % 2) != 0) {
                renameFile[i] = new File("filesForUpload/" + "x\\x" + files[i].getName());
                files[i].renameTo(renameFile[i]);
                System.out.println(files[i].getAbsolutePath());
                System.out.println(renameFile[i].getAbsolutePath());
            }
            else renameFile[i] = files[i];
        }
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(renameFile, randomFolderName, projectID, token, checkUpload).prettyPeek(), 207);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 5);
        sa.assertAll();
    }
*/
    //making 10 (maximum upload for now) temporary files, uploading them locally, deleting them after
    @Test
    public void shouldUploadTenFilesToProject() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(10);
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 10);
        sa.assertAll();
    }

    //making 1 temp file, uploading directly to project folder
    @Test
    public void shouldUploadOneFileToProject() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        sa.assertAll();
    }

    //should upload file that already exists in folder (adds (1) if it is first copy)
    @Test
    public void shouldUploadFileAlreadyExistsInFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        checkUpload.clear();
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),2);
        sa.assertAll();
    }

    //should upload file that already exists in project (adds (1) if it is the first copy)
    @Test
    public void shouldUploadFileAlreadyExistsInProject() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        checkUpload.clear();
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),2);
        sa.assertAll();
    }

    //invalid upload due to token being invalid
    @Test
    public void invalidUploadInvalidToken() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        String invalidToken = "invalidToken";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, invalidToken, checkUpload).statusCode(), 401);
        for (Boolean checkMe : checkUpload) sa.assertFalse(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),0);
        sa.assertAll();
    }

    //invalid upload due to token being empty
    @Test
    public void invalidUploadNoToken() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        String invalidToken1 = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, invalidToken1, checkUpload).statusCode(), 401);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),0);
        sa.assertAll();
    }

    //asserts that file of random user can't be found in DB because of exceeding maximum upload
    @Test
    public void invalidUploadExceedingMaxFiles() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(11);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),413);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 0);
        sa.assertAll();
    }

    @Test
    public void invalidUploadToInvalidFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFile.uploadFiles(files, "invalidFolder", projectID, token, checkUpload).statusCode(),404);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 0);
        sa.assertAll();
    }

    @AfterMethod
    public void tearDownMethod() {
        BoxProject.deleteProject(token, projectID);
        for (File file : files) file.deleteOnExit();
        checkUpload.clear();
    }
}