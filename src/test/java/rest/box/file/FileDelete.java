package rest.box.file;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxFolder;
import org.testng.annotations.*;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class FileDelete extends BaseRest {
    File[] files;
    ArrayList<Boolean> checkUpload = new ArrayList<>();
    ArrayList<Boolean> checkDelete = new ArrayList<>();

    @BeforeMethod
    public void prepareForTests(){
        randomFolderName = GenerateRandom.randomDataName();

    }

    //should create, upload, then delete file from folder
    @Test
    public void shouldDeleteFileFromFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals(BoxFile.deleteFiles(files, randomFolderName, projectID, token, checkDelete).statusCode(), 200);
        for (Boolean checkMe : checkDelete) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),0);
        sa.assertAll();
    }

    //should create, upload, then delete files from folder
    @Test
    public void shouldDeleteMultipleFilesFromFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(10);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),10);
        sa.assertEquals(BoxFile.deleteFiles(files, randomFolderName, projectID, token, checkDelete).statusCode(), 200);
        for (Boolean checkMe : checkDelete) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),0);
        sa.assertAll();
    }

    //should create, upload, then delete file from project
    @Test
    public void shouldDeleteFileFromProject() throws SQLException {
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals(BoxFile.deleteFiles(files, randomFolderName, projectID, token, checkDelete).statusCode(), 200);
        for (Boolean checkMe : checkDelete) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),0);
        sa.assertAll();
    }

    //should create, upload, then delete files from project
    @Test
    public void shouldDeleteMultipleFilesFromProject() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(10);
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 10);
        sa.assertEquals(BoxFile.deleteFiles(files, randomFolderName, projectID, token, checkDelete).statusCode(), 200);
        for (Boolean checkMe : checkDelete) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 0);
        sa.assertAll();
    }

    //invalid delete with invalid path to file
    @Test
    public void invalidDeleteFileInvalidPath() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        sa.assertEquals(BoxFile.deleteFiles(files, "invalidFolder", projectID, token, checkDelete).statusCode(), 400);
        for (Boolean checkMe : checkDelete) sa.assertFalse(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        sa.assertAll();
    }

    //invalid delete file with invalid project ID (can't be zero)
    @Test
    public void invalidDeleteFileInvalidProjectID() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe) ;
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        sa.assertEquals(BoxFile.deleteFiles(files, randomFolderName, 0, token, checkDelete).statusCode(), 404);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        sa.assertAll();
    }

    //invalid delete file with invalid project ID (can't be zero)
    @Test
    public void invalidDeleteFileInvalidToken() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe) ;
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        String invalidToken = "invalidToken";
        sa.assertEquals(BoxFile.deleteFiles(files, randomFolderName, projectID, invalidToken, checkDelete).statusCode(), 401);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID), 1);
        sa.assertAll();
    }

    @AfterMethod
    public void tearDownMethod() {
        BoxFile.deleteFiles(files, randomFolderName, projectID, token, checkDelete);
        for (File file : files) file.deleteOnExit();
        checkUpload.clear();
        checkDelete.clear();
    }
}
