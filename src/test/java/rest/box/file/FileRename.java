package rest.box.file;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.pojo.file.RenameRequestPojo;
import helpers.requests.BoxFile;
import static helpers.requests.BoxFolder.*;

import helpers.requests.BoxFolder;
import helpers.utils.UtilsForAPI;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class FileRename extends BaseRest {

    ArrayList<Boolean> checkUpload = new ArrayList<>();
    File[] files;
    String filePath;
    String newFilePath;
    @BeforeMethod
    public void prepareData(){
        sa = new SoftAssert();
        randomFileRename = (GenerateRandom.randomDataName()+".txt"); // adds extension while renaming files
    }

    //uploads file to folder, renames it, checks if it is renamed
    @Test
    public void shouldRenameFileInFolder() throws IOException {
        files = BoxFile.createRandomFiles(1);
        filePath = (randomFolderName + "/" + files[0].getName());
        sa.assertEquals(createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(rename(new RenameRequestPojo(filePath, randomFileRename), token, projectID).statusCode(), 200);
        sa.assertFalse(Files.exists(UtilsForAPI.findPath(email, projectID, filePath)));
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName, randomFileRename)));
        sa.assertAll();
    }

    //uploads file to project, renames it, checks if it is renamed
    @Test
    public void shouldRenameFileInProject() throws IOException {
        files = BoxFile.createRandomFiles(1);
        String randomFolderName1 = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName1, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(rename(new RenameRequestPojo(files[0].getName(), randomFileRename), token, projectID).statusCode(), 200);
        sa.assertFalse(Files.exists(UtilsForAPI.findPath(email, projectID,randomFolderName1, files[0].getName())));
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName1, randomFileRename)));
        sa.assertAll();
    }

    //uploads file to folder, renames it with a special blank character, checks if it is renamed
    @Test
    public void shouldRenameFileToBlankChar() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        String randomFileRename1 = "\u200E";
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[0].getName(), randomFileRename1), token, projectID).statusCode(), 400);
        sa.assertAll();
    }

    //invalid rename of file with and empty string
    @Test
    public void invalidRenameFileEmptyRename() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        String randomFileRename2 = "";
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[0].getName(), randomFileRename2), token, projectID).statusCode(), 400);
        sa.assertAll();
    }

    //invalid rename of file with and empty string
    @Test
    public void invalidRenameFileCannotContainSlash() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        String randomFileRename2 = "//";
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[0].getName(), randomFileRename2), token, projectID).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void invalidRenameFileCannotContainEmptySpaceAtEnd() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        String randomFileRename2 = "invalidName ";
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[0].getName(), randomFileRename2), token, projectID).statusCode(), 400);
        sa.assertAll();
    }

    //invalid rename of file with an already existing file name
    @Test
    public void invalidRenameFileExistingName() throws IOException {
        files = BoxFile.createRandomFiles(2);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[0].getName(), randomFileRename), token, projectID).statusCode(), 200);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName + "/" + randomFileRename)));
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[1].getName(), randomFileRename), token, projectID).statusCode(), 409);
        sa.assertAll();
    }

    //invalid rename of file with an invalid project ID (can't be zero)
    @Test
    public void invalidRenameFileInvalidProjectID() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        int wrongID = 0;
        sa.assertEquals(rename(new RenameRequestPojo(randomFolderName + "/" + files[0].getName(), randomFileRename), token, wrongID).statusCode(), 404);
        sa.assertAll();
    }

    //invalid rename of file with invalid token
    @Test
    public void invalidRenameFileInvalidToken() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        String invalidToken = "invalidToken";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        sa.assertEquals(rename(new RenameRequestPojo(files[0].getName(), randomFileRename), invalidToken, projectID).statusCode(), 401);
        sa.assertAll();
    }

    //invalid rename of file with empty token
    @Test
    public void invalidRenameFileNoToken() throws IOException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        String invalidToken2 = "";
        sa.assertEquals(rename(new RenameRequestPojo(files[0].getName(), randomFileRename), invalidToken2, projectID).statusCode(), 401);
        sa.assertAll();
    }

    @AfterMethod
    public void tearDownMethod() {
        BoxFolder.delete(randomFolderName, projectID, token);
        BoxFolder.delete(randomFolderRename, projectID, token);
        BoxFile.deleteFiles(files, randomFileName, projectID, token, checkUpload);
        BoxFile.deleteFiles(files, randomFileRename, projectID, token, checkUpload);
        for (File file : files) file.deleteOnExit();
        checkUpload.clear();
    }

}
