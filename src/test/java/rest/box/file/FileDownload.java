package rest.box.file;

import helpers.utils.UtilsForAPI;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFile;
import helpers.requests.BoxFolder;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;

public class FileDownload extends BaseRest {
    File[] files;
    ArrayList<Boolean> checkUpload = new ArrayList<>();
    ArrayList<Boolean> checkDelete = new ArrayList<>();

    @BeforeMethod
    public void makeFolder(){
        randomFolderName = GenerateRandom.randomDataName();
    }
    //uploads file to folder, writes a string in the txt, then downloads file from folder
    @Test
    public void shouldDownloadFileFromFolder() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(),200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals((BoxFile.downloadFiles(projectID, files, randomFolderName, token).statusCode()),200);
        sa.assertAll();
    }

    //uploads file to project, writes a string in the txt, then downloads file from project
    @Test
    public void shouldDownloadFileFromProject() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        randomFolderName = "";
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals(BoxFile.downloadFiles(projectID, files,"", token).statusCode(), 200);
        sa.assertAll();
    }

    //invalid download with invalid token
    @Test
    public void invalidDownloadFileInvalidToken() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        String invalidToken = "invalidToken";
        sa.assertEquals(BoxFile.downloadFiles(projectID, files, randomFolderName, invalidToken).statusCode(), 401);
        sa.assertAll();
    }

    //invalid download with empty token
    @Test
    public void invalidDownloadFileNoToken() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        String emptyToken = "";
        sa.assertEquals(BoxFile.downloadFiles(projectID, files, randomFolderName, emptyToken).statusCode(), 401);
        sa.assertAll();
    }

    //invalid download from invalid project ID
    @Test
    public void invalidDownloadFileInvalidProjectID() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        int invalidID = 0;
        sa.assertEquals(BoxFile.downloadFiles(invalidID, files, randomFolderName, token).statusCode(), 404);
        sa.assertAll();
    }

    //invalid download from folder that doesn't exist
    @Test
    public void invalidDownloadInvalidFilePath() throws IOException, SQLException {
        files = BoxFile.createRandomFiles(1);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(),201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload).statusCode(), 200);
        for (Boolean checkMe : checkUpload) sa.assertTrue(checkMe);
        sa.assertEquals(DataBaseManipulation.checkFiles(email, projectID),1);
        sa.assertEquals(BoxFile.downloadFiles(projectID, files, "invalidFolder", token).statusCode(), 404);
        sa.assertAll();
    }

    @AfterMethod
    public void deleteFromList(){
        BoxFile.deleteFiles(files, randomFolderName, projectID, token, checkDelete);
        for (File file : files) file.deleteOnExit();
        checkUpload.clear();
    }
}
