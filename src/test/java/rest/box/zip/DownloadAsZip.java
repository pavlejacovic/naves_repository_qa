package rest.box.zip;

import helpers.requests.*;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class DownloadAsZip extends BaseRest {

    int numberOfFiles = 10;
    File[] files = new File[numberOfFiles];
    ArrayList<Boolean> checkUpload = new ArrayList<>();

    @BeforeClass
    public void buildDataForTests() throws IOException {
        files = BoxFile.createRandomFiles(numberOfFiles);
        BoxFolder.createFolder(randomFolderName, projectID, token);
        BoxFile.uploadFiles(files, randomFolderName, projectID, token, checkUpload);
        BoxFile.uploadFiles(files, "", projectID, token, checkUpload);
        for (File file : files) file.deleteOnExit();
    }

    @Test
    public void shouldDownloadFilesFromFolderAsZip(){
        Response response = BoxZip.downloadFilesAsZip(files, randomFolderName, token, projectID);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertFalse(response.getBody().asString().contains(randomFolderName));
        for (File file : files) sa.assertTrue(response.getBody().asString().contains(file.getName()));
        sa.assertAll();
    }

    @Test
    public void shouldDownloadFilesFromRootAsZip(){
        Response response = BoxZip.downloadFilesAsZip(files, "", token, projectID);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertFalse(response.getBody().asString().contains(randomFolderName));
        for (File file : files) sa.assertTrue(response.getBody().asString().contains(file.getName()));
        sa.assertAll();
    }

    @Test
    public void shouldDownloadProjectAsZip(){
        Response response = BoxZip.downloadProjectAsZip(token, projectID);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(response.getBody().asString().contains(randomFolderName));
        for (File file : files) sa.assertTrue(response.getBody().asString().contains(file.getName()));
        sa.assertAll();
    }

    @Test
    public void shouldDownloadFolderAsZip(){
        Response response = BoxZip.downloadFolderAsZip(randomFolderName, token, projectID);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(response.getBody().asString().contains(randomFolderName));
        for (File file : files) sa.assertTrue(response.getBody().asString().contains(file.getName()));
        sa.assertAll();
    }
}