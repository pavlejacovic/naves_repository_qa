//Sasa Avramovic
package rest.box.project;

import helpers.Constants;
import helpers.utils.Property;
import helpers.generators.GenerateRandom;
import helpers.dataBase.DataBaseManipulation;
import helpers.requests.BoxProject;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import static io.restassured.RestAssured.given;

public class CreateProject extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        randomProjectName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldCreateProject(){
        BoxProject.deleteProject(token, projectID);
        projectBody.addProperty("projectName", randomProjectName);
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName));
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectWithBlankName(){
        projectBody.addProperty("projectName", "");
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkProject(email, ""));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectWithoutToken(){
        projectBody.addProperty("projectName", randomProjectName);
        sa.assertEquals(BoxProject.createProject(projectBody, "").statusCode(), 401);
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectWithInvalidToken(){
        projectBody.addProperty("projectName", "");
        sa.assertEquals(BoxProject.createProject(projectBody, "InvalidTokenString").statusCode(), 401);
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectWithSpaceName(){
        projectBody.addProperty("projectName", " ");
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkProject(email, " "));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectExistingName(){
        projectBody.addProperty("projectName", randomProjectName);
        BoxProject.createProject(projectBody, token);
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 409);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateProjectNameTooLong(){
        projectBody.addProperty("projectName", Constants.invalidNameTooLong);
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkProject(email, Constants.invalidNameTooLong));
        sa.assertAll();
    }

    @AfterMethod
    public void deleteProject(){
        BoxProject.deleteProject(token, projectID);
    }
}