package rest.box.project;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxProject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.BaseRest;

public class ListProjects extends BaseRest {

    int numberOfProjects = 10;
    int[] projectID = new int[numberOfProjects];

    @BeforeClass
    public void registerAndLogInUser() {
        //create projects
        for (int i = 0; i < numberOfProjects; i++){
            randomProjectName = GenerateRandom.randomDataName();
            projectBody.addProperty("projectName", randomProjectName);
            BoxProject.createProject(projectBody, token);
            DataBaseManipulation.checkProject(email, randomProjectName);
            projectID[i] = DataBaseManipulation.getProjectID(email, randomProjectName);
        }
    }

    @Test
    public void shouldListProjects(){
        Assert.assertEquals(BoxProject.listProjects(token).statusCode(), 200);
    }

    @Test
    public void shouldNotListProjectsNoToken(){
        Assert.assertEquals(BoxProject.listProjects("").statusCode(), 401);
    }

    @Test
    public void shouldNotListProjectsInvalidToken(){
        Assert.assertEquals(BoxProject.listProjects("randomTokenString").statusCode(), 401);
    }

    @AfterClass
    public void tearDownClass(){
        for (int i = 0; i < numberOfProjects; i++){
            BoxProject.deleteProject(token, projectID[i]);
        }
    }
}