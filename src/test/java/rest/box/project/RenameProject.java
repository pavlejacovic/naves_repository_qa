package rest.box.project;

import helpers.Constants;
import helpers.utils.Property;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxProject;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

public class RenameProject extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        BoxProject.deleteProject(token, projectID);
        randomProjectName = GenerateRandom.randomDataName();
        randomProjectRename = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldRenameProject(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, token, projectID).statusCode(), 200, "project rename");
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does not exist");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectRename), "new project name exists");
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectBlankName(){
        randomProjectRename = "";
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, token, projectID).statusCode(), 400, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertFalse(DataBaseManipulation.checkProject(email, ""), "new project name does not exist");
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectSpaceName(){
        randomProjectRename = " ";
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, token, projectID).statusCode(), 400, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertFalse(DataBaseManipulation.checkProject(email, ""), "new project name does not exist");
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectInvalidToken(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, "randomTokenString", projectID).statusCode(), 401, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectRename), "new project name does not exist");
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectNoToken(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, "", projectID).statusCode(), 401, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectRename), "new project name does not exist");
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectExistingName(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //create second project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectRename), "project exists");
        int projectID2 = DataBaseManipulation.getProjectID(email, randomProjectRename);
        //rename first project
        sa.assertEquals(BoxProject.renameProject(projectBody, token, projectID).statusCode(), 409, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectRename), "new project name already exists");
        sa.assertAll();
        BoxProject.deleteProject(token, projectID);
        BoxProject.deleteProject(token, projectID2);
    }

    @Test
    public void shouldNotRenameProjectLongName(){
        randomProjectRename = Constants.invalidNameTooLong;
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, token, projectID).statusCode(), 400, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectRename), "new project name does not exist");
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameProjectInvalidProjectID(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        int projectID2 = 0;
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        projectBody.addProperty("projectName", randomProjectRename);
        //rename project
        sa.assertEquals(BoxProject.renameProject(projectBody, token, projectID2).statusCode(), 404, "project rename");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "old project name does exist");
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectRename), "new project name does not exist");
        sa.assertAll();
        BoxProject.deleteProject(token, projectID);
    }

    @AfterMethod
    public void deleteProject(){
       BoxProject.deleteProject(token, projectID);
    }
}
