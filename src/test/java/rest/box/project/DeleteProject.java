package rest.box.project;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxProject;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import static io.restassured.RestAssured.given;

public class DeleteProject extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        randomProjectName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldDeleteProject(){
        BoxProject.deleteProject(token, projectID);
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        //delete project
        sa.assertEquals(BoxProject.deleteProject(token, projectID).statusCode(), 200, "project delete");
        sa.assertFalse(DataBaseManipulation.checkProject(email, randomProjectName), "project name does not exist");
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteProjectInvalidID(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        int projectID2 = 0;
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        //delete project
        sa.assertEquals(BoxProject.deleteProject(token, projectID2).statusCode(), 404, "project not deleted");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project name does exist");
        sa.assertAll();
        BoxProject.deleteProject(token, projectID);
    }

    @Test
    public void shouldNotDeleteProjectNoToken(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        //delete project
        sa.assertEquals(BoxProject.deleteProject("", projectID).statusCode(), 401, "project not deleted");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project name does exist");
        sa.assertAll();
        BoxProject.deleteProject(token, projectID);
    }

    @Test
    public void shouldNotDeleteProjectInvalidToken(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        //delete project
        sa.assertEquals(BoxProject.deleteProject("randomTokenString", projectID).statusCode(), 401, "project not deleted");
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project name does exist");
        sa.assertAll();
        BoxProject.deleteProject(token, projectID);
    }

    @Test
    public void shouldNotDeleteProjectInvalidPath(){
        projectBody.addProperty("projectName", randomProjectName);
        //create project
        sa.assertEquals(BoxProject.createProject(projectBody, token).statusCode(), 201, "project created");
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project exists");
        //delete project, done step by step for this test only
        given().
                header("Content-Type", "application/json").
                header("Authorization", token).
                when().
                delete("/box/projects/../" + projectID).
                then().
                statusCode(401);
        sa.assertTrue(DataBaseManipulation.checkProject(email, randomProjectName), "project name does exist");
        sa.assertAll();
        BoxProject.deleteProject(token, projectID);
    }
}
