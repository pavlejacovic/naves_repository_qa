package rest.box.project;

import com.google.gson.JsonObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.UserBuilder;
import helpers.pojo.project.ListAllSharedPojo;
import helpers.pojo.project.ShareProjectRequestPojo;
import helpers.pojo.project.ShareProjectResponsePojo;
import helpers.requests.Authentication;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import helpers.utils.GetUserPropertyFromJson;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ShareProject extends BaseRest {

    Response response;
    File[] testFiles;
    ArrayList<Boolean> check = new ArrayList<>();
    ShareProjectRequestPojo[] requestPojo;
    ShareProjectResponsePojo[] responsePojo;
    ListAllSharedPojo[] listPojo;
    JsonObject[] tempUser = new JsonObject[10];

    @BeforeClass
    public void prepareDataForTests() throws IOException {
        testFiles = BoxFile.createRandomFiles(10);
        for (int i = 0; i < 10; i++) {
            tempUser[i] = UserBuilder.testUser();
            Authentication.registerLoginReturnToken(tempUser[i]);
        }
    }

    @Test
    public void shouldShareWithOneUserWrite(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "WRITE");
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(responsePojo[0].getSuccessful());
        //should upload files to shared project
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token1, check).statusCode(), 200);
        //should download files from shared project
        sa.assertEquals(BoxFile.downloadFiles(projectID, testFiles, "", token1).statusCode(), 200);
        //should delete files from shared project
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token1, check).statusCode(), 200);
        sa.assertAll();
        //remove permissions
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
    }

    @Test
    public void shouldShareWithOneUserReadOnly(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "READ_ONLY");
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(responsePojo[0].getSuccessful());
        //should not be able to upload files to shared project
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token1, check).statusCode(), 403);
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token, check).statusCode(), 200);
        //should be able to download files from shared project
        sa.assertEquals(BoxFile.downloadFiles(projectID, testFiles, "", token1).statusCode(), 200);
        //should not be able to delete files from shared project
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token1, check).statusCode(), 403);
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token, check).statusCode(), 200);
        sa.assertAll();
        //remove permissions
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
    }

    @Test
    public void shouldRemoveWritePermission(){
        //add write permission
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "WRITE");
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(responsePojo[0].getSuccessful());
        //remove write permission
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(responsePojo[0].getSuccessful());
        //should not upload files without permissions
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token1, check).statusCode(), 403);
        //should not download files without permissions
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token, check).statusCode(), 200);
        sa.assertEquals(BoxFile.downloadFiles(projectID, testFiles, "", token1).statusCode(), 403);
        //should not delete files without permissions
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token1, check).statusCode(), 403);
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token, check).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldRemoveReadOnlyPermission(){
        //add write permission
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "READ_ONLY");
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(responsePojo[0].getSuccessful());
        //remove write permission
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertTrue(responsePojo[0].getSuccessful());
        //should not upload files without permissions
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token1, check).statusCode(), 403);
        //should not download files without permissions
        sa.assertEquals(BoxFile.uploadFiles(testFiles, "", projectID, token, check).statusCode(), 200);
        sa.assertEquals(BoxFile.downloadFiles(projectID, testFiles, "", token1).statusCode(), 403);
        //should not delete files without permissions
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token1, check).statusCode(), 403);
        sa.assertEquals(BoxFile.deleteFiles(testFiles, "", projectID, token, check).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotShareInvalidExistingToken(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "WRITE");
        response = BoxProject.shareProject(token1, projectID, requestPojo);
        sa.assertEquals(response.statusCode(), 403);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "READ_ONLY");
        response = BoxProject.shareProject(token1, projectID, requestPojo);
        sa.assertEquals(response.statusCode(), 403);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
        response = BoxProject.shareProject(token1, projectID, requestPojo);
        sa.assertEquals(response.statusCode(), 403);
        sa.assertAll();
    }


    @Test
    public void shouldNotShareNoToken(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "WRITE");
        response = BoxProject.shareProject("", projectID, requestPojo);
        sa.assertEquals(response.statusCode(), 401);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "READ_ONLY");
        response = BoxProject.shareProject("", projectID, requestPojo);
        sa.assertEquals(response.statusCode(), 401);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
        response = BoxProject.shareProject("", projectID, requestPojo);
        sa.assertEquals(response.statusCode(), 401);
        sa.assertAll();
    }

    @Test
    public void shouldNotShareInvalidProjectID(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "WRITE");
        response = BoxProject.shareProject(token, 0, requestPojo);
        sa.assertEquals(response.statusCode(), 404);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "READ_ONLY");
        response = BoxProject.shareProject(token, 0, requestPojo);
        sa.assertEquals(response.statusCode(), 404);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
        response = BoxProject.shareProject(token, 0, requestPojo);
        sa.assertEquals(response.statusCode(), 404);
        sa.assertAll();
    }

    @Test
    public void shouldShareWithTenUsersReadWriteDelete(){
        requestPojo = new ShareProjectRequestPojo[10];
        for (int i = 0; i < 10; i++){
            //add write permissions to request for some users
            if (i % 2 == 0) requestPojo[i] = new ShareProjectRequestPojo(GetUserPropertyFromJson.getEmail(tempUser[i]), "WRITE");
            //add read only to request for some users
            else requestPojo[i] = new ShareProjectRequestPojo(GetUserPropertyFromJson.getEmail(tempUser[i]), "READ_ONLY");
        }
        //add permissions
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        for (int i = 0; i < 10; i++){
            sa.assertTrue(responsePojo[i].getSuccessful());
            //remove permissions in request
            requestPojo[i].setPermission("DELETE");
        }
        //remove permissions
        response = BoxProject.shareProject(token, projectID, requestPojo);
        responsePojo = response.getBody().as(ShareProjectResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        for (int i = 0; i < 10; i++) {
            sa.assertTrue(responsePojo[i].getSuccessful());
        }
        sa.assertAll();
    }

    @Test
    public void shouldListAllUsersWithPermissions(){
        requestPojo = new ShareProjectRequestPojo[10];
        for (int i = 0; i < 10; i++){
            //add write permissions to request for some users
            if (i % 2 == 0) requestPojo[i] = new ShareProjectRequestPojo(GetUserPropertyFromJson.getEmail(tempUser[i]), "WRITE");
                //add read only to request for some users
            else requestPojo[i] = new ShareProjectRequestPojo(GetUserPropertyFromJson.getEmail(tempUser[i]), "READ_ONLY");
        }
        BoxProject.shareProject(token, projectID, requestPojo);
        response = BoxProject.listSharedUsers(token, projectID);
        listPojo = response.getBody().as(ListAllSharedPojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        for (int i = 0; i < 10; i++){
            sa.assertEquals(listPojo[i].getProject().getOwner().getEmail(), email);
            sa.assertEquals(listPojo[i].getUser().getEmail(), GetUserPropertyFromJson.getEmail(tempUser[i]));
            if (i % 2 == 0) sa.assertTrue(listPojo[i].getWritePermission());
            else sa.assertFalse(listPojo[i].getWritePermission());
        }
        sa.assertAll();
    }

    @Test
    public void shouldNotListAllUsersWithPermissionsInvalidToken(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(GetUserPropertyFromJson.getEmail(tempUser[0]), "WRITE");
        BoxProject.shareProject(token, projectID, requestPojo);
        response = BoxProject.listSharedUsers("randomTokenString", projectID);
        sa.assertEquals(response.statusCode(), 401);
        requestPojo[0] = new ShareProjectRequestPojo(GetUserPropertyFromJson.getEmail(tempUser[0]), "DELETE");
        BoxProject.shareProject(token, projectID, requestPojo);
        sa.assertAll();
    }

    @Test
    public void shouldNotListAllUsersWithPermissionsWrongUserToken(){
        requestPojo = new ShareProjectRequestPojo[1];
        requestPojo[0] = new ShareProjectRequestPojo(email1, "WRITE");
        BoxProject.shareProject(token, projectID, requestPojo);
        response = BoxProject.listSharedUsers(token1, projectID);
        sa.assertEquals(response.statusCode(), 403);
        requestPojo[0] = new ShareProjectRequestPojo(email1, "DELETE");
        BoxProject.shareProject(token, projectID, requestPojo);
        sa.assertAll();
    }

    @AfterClass
    public void removeTempUsers(){
        for (int i = 0; i < 10; i++){
            DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(tempUser[i]));
        }
    }
}