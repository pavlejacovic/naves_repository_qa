package rest.box.folder;

import helpers.Constants;
import helpers.utils.Property;
import helpers.utils.UtilsForAPI;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFolder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.nio.file.Files;

import static io.restassured.RestAssured.given;

public class CreateFolder extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        randomFolderName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldCreateFolder(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201, "create folder");
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200, "delete folder");
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderWithExistingName(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 409);
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderInvalidID(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, 0, token).statusCode(), 404);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, 0, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderInvalidToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, "randomTokenString").statusCode(), 401);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderNoToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, "").statusCode(), 401);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderBlankFolderName(){
        randomFolderName = "";
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderInvalidPath(){
        randomFolderName = "/../" + GenerateRandom.randomDataName();
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 400);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateFolderNameTooLong(){
        randomFolderName = Constants.invalidNameTooLong;
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 500);
        sa.assertAll();
    }
}