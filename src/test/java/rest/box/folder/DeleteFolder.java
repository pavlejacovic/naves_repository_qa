package rest.box.folder;

import com.google.gson.JsonObject;
import helpers.utils.UtilsForAPI;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFolder;
import helpers.requests.BoxProject;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.nio.file.Files;

public class DeleteFolder extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        randomFolderName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldDeleteFolder(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldDeleteMultipleFolders(){
        String[] randomFolders = new String[50];
        for (int i = 0; i < randomFolders.length; i++) {
            randomFolders[i] = GenerateRandom.randomDataName();
            sa.assertEquals(BoxFolder.createFolder(randomFolders[i], projectID, token).statusCode(), 201, "create random folder");
            sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolders[i])));
        }
        sa.assertEquals(BoxFolder.delete(randomFolders, projectID, token).statusCode(), 200, "delete all folders");
        for (String randomFolder : randomFolders) {
            sa.assertFalse(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolder)));
        }
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteFolderInvalidProject(){
        int testProjectID;
        JsonObject testProjectBody = new JsonObject();
        testProjectBody.addProperty("projectName", "testProjectName");
        BoxProject.createProject(testProjectBody, token);
        testProjectID = DataBaseManipulation.getProjectID(email, "testProjectName");
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, testProjectID, token).statusCode(), 201);
        sa.assertEquals(BoxFolder.delete("../" + testProjectID + "/" + randomFolderName, projectID, token).statusCode(), 400);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, testProjectID, randomFolderName)));
        sa.assertAll();
        BoxProject.deleteProject(token, testProjectID);
    }

    @Test
    public void shouldNotDeleteProjectRootFolder(){
        sa.assertEquals(BoxFolder.delete("/", projectID, token).statusCode(), 400);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, "")));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteFolderInvalidID(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, 0, token).statusCode(), 404);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteFolderInvalidPath(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.delete("wrongFolderName", projectID, token).statusCode(), 400);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteFolderInvalidToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, "randomTokenString").statusCode(), 401);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteFolderNoToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, "").statusCode(), 401);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertAll();
    }
}
