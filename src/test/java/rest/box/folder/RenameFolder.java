package rest.box.folder;

import helpers.pojo.file.RenameRequestPojo;
import helpers.utils.UtilsForAPI;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFolder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.nio.file.Files;

public class RenameFolder extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        randomFolderName = GenerateRandom.randomDataName();
        randomFolderRename = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldRenameFolder(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), token, projectID).statusCode(), 200);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.delete(randomFolderRename, projectID, token).statusCode(), 200);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertAll();
    }

    @Test
    public void shouldRenameFolderToHidden(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        randomFolderRename = "." + randomFolderName;
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), token, projectID).statusCode(), 200);
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.delete(randomFolderRename, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderRoot(){
        randomFolderName = "/";
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), token, projectID).statusCode(), 400);
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderExistingName(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.createFolder(randomFolderRename, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), token, projectID).statusCode(), 409);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertEquals(BoxFolder.delete(randomFolderRename, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderInvalidID(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), token, 0).statusCode(), 404);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderInvalidToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), "randomTokenString", projectID).statusCode(), 401);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotRenameFolderNoToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.rename(new RenameRequestPojo(randomFolderName, randomFolderRename), "randomTokenString", projectID).statusCode(), 401);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertTrue(Files.notExists(UtilsForAPI.findPath(email, projectID, randomFolderRename)));
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }
}