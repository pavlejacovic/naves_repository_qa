package rest.box.folder;

import helpers.utils.UtilsForAPI;
import helpers.generators.GenerateRandom;
import helpers.requests.BoxFolder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import rest.BaseRest;

import java.nio.file.Files;

public class ListFoldersAndFiles extends BaseRest {

    @BeforeMethod
    public void prepareData(){
        randomFolderName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldListContents(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201, "create Folder");
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.listAll("", projectID, token).statusCode(), 200, "list All");
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200, "delete");
        sa.assertAll();
    }

    @Test
    public void shouldNotListContentsInvalidID(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.listAll("", 0, token).statusCode(), 404);
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotListContentsInvalidToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.listAll("", projectID, "randomTokenString").statusCode(), 401);
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotListContentsNoToken(){
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.listAll("", projectID, "").statusCode(), 401);
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotListContentsInvalidPath(){
        String randomTestFolderName = GenerateRandom.randomDataName();
        sa.assertEquals(BoxFolder.createFolder(randomFolderName, projectID, token).statusCode(), 201);
        sa.assertTrue(Files.exists(UtilsForAPI.findPath(email, projectID, randomFolderName)));
        sa.assertEquals(BoxFolder.listAll(randomTestFolderName, projectID, token).statusCode(), 404);
        sa.assertEquals(BoxFolder.delete(randomFolderName, projectID, token).statusCode(), 200);
        sa.assertAll();
    }
}
