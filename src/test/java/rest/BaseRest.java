package rest;

import com.google.gson.JsonObject;
import helpers.ReadProperties;
import helpers.utils.GetUserPropertyFromJson;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import helpers.requests.BoxProject;
import helpers.utils.Property;
import io.restassured.RestAssured;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

public class BaseRest {

    public JsonObject testUser = new JsonObject();
    public JsonObject testUser1 = new JsonObject();
    public JsonObject validate = new JsonObject();
    public JsonObject resetPasswordBody = new JsonObject();
    public JsonObject projectBody = new JsonObject();
    public String email;
    public String email1;
    public String password;
    public String validationToken;
    public String passwordResetToken;
    public String logInToken;
    public String token;
    public String token1;
    public String randomProjectName;
    public String randomProjectRename;
    public String randomFolderName;
    public String randomFolderRename;
    public String randomFileName;
    public String randomFileRename;
    public String randomFunctionName;
    public String randomFunctionRename;
    public String language;
    public int projectID;
    public SoftAssert sa = new SoftAssert();

    @BeforeClass
    public void prepareTests(){
        language = "JAVA";
        RestAssured.baseURI = Property.getProperty("baseUri");
        testUser = UserBuilder.testUser();
        testUser1 = UserBuilder.testUser();
        email = GetUserPropertyFromJson.getEmail(testUser);
        email1 = GetUserPropertyFromJson.getEmail(testUser1);
        token = Authentication.registerLoginReturnToken(testUser);
        token1 = Authentication.registerLoginReturnToken(testUser1);
        password = GetUserPropertyFromJson.getPassword(testUser);
        randomProjectName = GenerateRandom.randomDataName();
        projectBody.addProperty("projectName", randomProjectName);
        BoxProject.createProject(projectBody, token);
        projectID = DataBaseManipulation.getProjectID(email, randomProjectName);
        randomFolderName = GenerateRandom.randomDataName();
        randomFileName = GenerateRandom.randomDataName();
    }

    @BeforeMethod
    public void resetSoftAssure(){
        sa = new SoftAssert();
    }

    @AfterClass
    public void tearDown(){
        BoxProject.deleteProject(token, projectID);
        DataBaseManipulation.deleteUser(email);
        DataBaseManipulation.deleteUser(email1);
    }
}