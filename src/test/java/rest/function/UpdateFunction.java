package rest.function;

import com.google.gson.JsonObject;
import helpers.Constants;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.pojo.function.UpdateFunctionResponsePojo;
import helpers.requests.Authentication;
import helpers.requests.BoxFile;
import helpers.requests.NavesFunction;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;

public class UpdateFunction extends BaseRest {

    File file1;
    File file2;
    File file3;
    String trigger;
    boolean publicAccess;
    Integer[] prID = new Integer[1];
    Integer[] tempID = new Integer[1];
    Response response;
    UpdateFunctionResponsePojo responsePojo = new UpdateFunctionResponsePojo();

    @BeforeClass
    public void prepareData() throws IOException {
        file1 = BoxFile.createRandomFile(".jar");
        file1.deleteOnExit();
        file2 = BoxFile.createRandomFile(".jar");
        file2.deleteOnExit();
        file3 = BoxFile.createRandomFile(".txt");
        file3.deleteOnExit();
        prID[0] = DataBaseManipulation.getProjectID(email, randomProjectName);
    }

    @BeforeMethod
    public void prepareForTest(){
        randomFunctionName = GenerateRandom.randomDataName();
        randomFunctionRename = GenerateRandom.randomDataName();
        trigger = "GET";
        publicAccess = true;
    }

    @Test
    public void shouldUpdateFunctionNoProjectID(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, null);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        response = NavesFunction.updateFunctionProperties(token, randomFunctionRename, false, "POST", null, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA");
        responsePojo = response.getBody().as(UpdateFunctionResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getFunctionName(), randomFunctionRename + "-" + DataBaseManipulation.getFromDatabase(email, "user_id"));
        sa.assertEquals(responsePojo.getTrigger(), "POST");
        sa.assertFalse(responsePojo.getPublicAccess());
        sa.assertEquals(responsePojo.getProjectIds().toString(), "[]");
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionRename));
        sa.assertAll();
    }

    @Test
    public void shouldUpdateFunctionValidProjectID(){
        trigger = "BUCKET_DOWNLOAD_FILE";
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, null);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        response = NavesFunction.updateFunctionProperties(token, randomFunctionRename, false, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA");
        responsePojo = response.getBody().as(UpdateFunctionResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getFunctionName(), randomFunctionRename + "-" + DataBaseManipulation.getFromDatabase(email, "user_id"));
        sa.assertEquals(responsePojo.getTrigger(), trigger);
        sa.assertFalse(responsePojo.getPublicAccess());
        sa.assertEquals(responsePojo.getProjectIds().toString(), "[" + prID[0] + "]");
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionRename));
        sa.assertAll();
    }

    @Test
    public void shouldUpdateFunctionInvalidProjectID(){
        trigger = "BUCKET_DOWNLOAD_FILE";
        tempID[0] = 0;
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        response = NavesFunction.updateFunctionProperties(token, randomFunctionRename, false, trigger, tempID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA");
        responsePojo = response.getBody().as(UpdateFunctionResponsePojo.class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.getFunctionName(), randomFunctionRename + "-" + DataBaseManipulation.getFromDatabase(email, "user_id"));
        sa.assertEquals(responsePojo.getTrigger(), trigger);
        sa.assertFalse(responsePojo.getPublicAccess());
        sa.assertEquals(responsePojo.getProjectIds().toString(), "[]");
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionRename));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateFunctionInvalidFunctionName(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        randomFunctionRename = Constants.invalidFunctionNameTooLong;
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        randomFunctionRename = "";
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        randomFunctionRename = null;
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        randomFunctionRename = "InvalidName";
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        randomFunctionRename = "invalidname!@#";
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateFunctionInvalidTrigger(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        trigger = "invalid";
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        trigger = null;
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateFunctionInvalidToken(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.updateFunctionProperties("randomTokenString", randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 401);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        sa.assertEquals(NavesFunction.updateFunctionProperties("", randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 401);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateFunctionUnauthorizedToken(){
        JsonObject testUser2 = UserBuilder.testUser();
        String token2 = Authentication.registerLoginReturnToken(testUser2);
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.updateFunctionProperties(token2, randomFunctionRename, publicAccess, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 403);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionRename));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(testUser2));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateFunctionInvalidPublicAccess(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertEquals(NavesFunction.updateFunctionProperties(token, randomFunctionRename, null, trigger, prID, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldUpdateExecution(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertEquals(NavesFunction.updateNavesFunctionExecution(token, file2, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 200);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateExecutionInvalidFile(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertEquals(NavesFunction.updateNavesFunctionExecution(token, file3, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 400);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateExecutionInvalidToken(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertEquals(NavesFunction.updateNavesFunctionExecution("randomTokenString", file2, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 401);
        sa.assertEquals(NavesFunction.updateNavesFunctionExecution("", file2, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 401);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateExecutionUnauthorizedToken(){
        JsonObject testUser2 = UserBuilder.testUser();
        String token2 = Authentication.registerLoginReturnToken(testUser2);
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertEquals(NavesFunction.updateNavesFunctionExecution(token2, file2, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), "JAVA").statusCode(), 403);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(testUser2));
        sa.assertAll();
    }

    @Test
    public void shouldNotUpdateExecutionInvalidFunctionID(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file1, language, prID);
        sa.assertEquals(NavesFunction.updateNavesFunctionExecution(token, file2, 0, "JAVA").statusCode(), 404);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }
}