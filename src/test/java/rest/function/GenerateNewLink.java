package rest.function;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.google.gson.JsonObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import helpers.requests.BoxFile;
import helpers.requests.NavesFunction;
import helpers.utils.Property;
import helpers.utils.UtilsForAPI;
import io.restassured.RestAssured;
import org.awaitility.Awaitility;
import org.testcontainers.DockerClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// testing generation of new link for execution of Naves function
public class GenerateNewLink extends BaseRest {
    File file;
    String trigger;
    boolean publicAccess;
    Integer[] prID = new Integer[1];
    String link;
    String link2;
    String parameter = "name";
    String value = "value";
    String method = "GET";
    String contentType = "multipart/form-data";
    File executableJar;
    int functionID;
    DockerClient dockerClient = DockerClientFactory.instance().client();
    List<Container> allContainers = new ArrayList<>();

    @BeforeClass
    public void prepareData() throws IOException {
        file = BoxFile.createRandomFile(".jar");
        executableJar = new File ("jarsForTests/renderHTML.jar");
        file.deleteOnExit();
        trigger = "GET";
        publicAccess = true;
        prID[0] = 1;
    }

    @BeforeMethod
    public void prepareForTest(){
        randomFunctionName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldGenerateNewLink(){
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.generateNewLink(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        link2 = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertNotEquals(link, link2);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    public void shouldNotGenerateUnauthorizedToken(){
        JsonObject testUser2 = UserBuilder.testUser();
        String token2 = Authentication.registerLoginReturnToken(testUser2);
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.generateNewLink(token2, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 403);
        link2 = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(link, link2);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotGenerateNewLinkInvalidToken(){
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.generateNewLink("invalidToken", DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 401);
        link2 = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(link, link2);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotGenerateNewLinkInvalidFunctionID(){
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.generateNewLink(token, 0).statusCode(), 404);
        link2 = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(link, link2);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldGenerateNewLinkAndExecute(){
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.generateNewLink(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        link2 = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertNotEquals(link, link2);
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link2, contentType).statusCode(), 200);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }
}