package rest.function;

import com.google.gson.JsonObject;
import helpers.Constants;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import helpers.requests.BoxFile;
import helpers.requests.NavesFunction;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import io.restassured.RestAssured;
import org.openqa.selenium.json.Json;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;

public class CreateAndDeleteFunction extends BaseRest {

    File file;
    String trigger;
    boolean publicAccess;
    String token2;
    JsonObject testUser2;
    Integer[] prID = new Integer[1];

    @BeforeClass
    public void prepareData() throws IOException {
        RestAssured.baseURI = Property.getProperty("noGateway");
        file = BoxFile.createRandomFile(".jar");
        file.deleteOnExit();
        trigger = "GET";
        publicAccess = true;
        prID[0] = 1;
    }

    @BeforeMethod
    public void prepareForTest(){
        randomFunctionName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldCreateNavesFunction(){
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 201);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateNavesFunctionLongName(){
        randomFunctionName = Constants.invalidFunctionNameTooLong;
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateNavesFunctionInvalidName(){
        randomFunctionName = "!";
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        randomFunctionName = "A";
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        randomFunctionName = "";
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        randomFunctionName = null;
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateNavesFunctionInvalidToken(){
        sa.assertEquals(NavesFunction.createNavesFunction("randomTokenString", randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 401);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertAll();
    }


    @Test
    public void shouldNotCreateNavesFunctionBlankParameter(){
        sa.assertEquals(NavesFunction.createNavesFunction("", randomFunctionName, publicAccess, trigger, file, language, prID).statusCode(), 401);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.createNavesFunction(token, "", publicAccess, trigger, file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, ""));
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, "", file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotCreateNavesFunctionInvalidTrigger(){
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, "DELETE", file, language, prID).statusCode(), 400);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldDeleteNavesFunction(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        sa.assertFalse(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteNavesFunctionInvalidToken(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.deleteNavesFunction("randomTokenString", DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 401);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteNavesFunctionUnauthorizedToken(){
        RestAssured.baseURI = Property.getProperty("baseUri");
        testUser2 = UserBuilder.testUser();
        token2 = Authentication.registerLoginReturnToken(testUser2);
        RestAssured.baseURI = Property.getProperty("noGateway");
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.deleteNavesFunction(token2, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 403);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        DataBaseManipulation.deleteUser(GetUserPropertyFromJson.getEmail(testUser2));
        sa.assertAll();
    }

    @Test
    public void shouldNotDeleteNavesFunctionInvalidFunctionID(){
        NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, file, language, prID);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        sa.assertEquals(NavesFunction.deleteNavesFunction(token, 0).statusCode(), 404);
        sa.assertTrue(DataBaseManipulation.checkFunction(email, randomFunctionName));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @AfterClass
    public void changeBaseUri(){
        RestAssured.baseURI = Property.getProperty("baseUri");
    }
}