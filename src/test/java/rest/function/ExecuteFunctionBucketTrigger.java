package rest.function;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.DockerObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.requests.BoxFile;
import helpers.requests.BoxProject;
import helpers.requests.NavesFunction;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import helpers.utils.UtilsForAPI;
import io.restassured.RestAssured;
import org.awaitility.Awaitility;
import org.testcontainers.DockerClientFactory;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.DockerStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.awt.event.ContainerListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class ExecuteFunctionBucketTrigger extends BaseRest {

    String triggerUpload = "BUCKET_UPLOAD_FILE";
    String triggerUploadFunction = "triggeruploadfunction";
    String triggerDownload = "BUCKET_DOWNLOAD_FILE";
    String triggerDownloadFunction = "triggerdownloadfunction";
    String triggerDelete = "BUCKET_DELETE_FILE";
    String triggerDeleteFunction = "triggerdeletefunction";
    File executableJar = new File ("jarsForTests/Wait.jar");
    File[] files = new File[1];
    Integer[] prID = new Integer[1];
    boolean publicAccess = true;
    ArrayList<Boolean> checkUpload = new ArrayList<>();
    DockerClient dockerClient = DockerClientFactory.instance().client();
    List<Container> allContainers = new ArrayList<>();

    @BeforeClass
    public void prepareFunctions() throws IOException {
        prID[0] = projectID;
        files[0] = BoxFile.createRandomFile("txt");
        files[0].deleteOnExit();
        NavesFunction.createNavesFunction(token, triggerUploadFunction, publicAccess, triggerUpload, executableJar, language, prID);
        NavesFunction.createNavesFunction(token, triggerDownloadFunction, publicAccess, triggerDownload, executableJar, language, prID);
        NavesFunction.createNavesFunction(token, triggerDeleteFunction, publicAccess, triggerDelete, executableJar, language, prID);
    }

    @Test
    public void shouldTriggerOnActionOwnedProject() {
        BoxFile.uploadFiles(files, "", projectID, token, checkUpload);
        Awaitility.await().until(UtilsForAPI.containerStarted(dockerClient, allContainers, email, triggerUploadFunction));
        sa.assertTrue(allContainers.get(0).getImage().equals(triggerUploadFunction + "-" + DataBaseManipulation.getFromDatabase(email, "user_id")));
        BoxFile.downloadFiles(projectID, files, "", token);
        Awaitility.await().until(UtilsForAPI.containerStarted(dockerClient, allContainers, email, triggerDownloadFunction));
        sa.assertTrue(allContainers.get(0).getImage().equals(triggerDownloadFunction + "-" + DataBaseManipulation.getFromDatabase(email, "user_id")));
        BoxFile.deleteFiles(files, "", projectID, token, checkUpload);
        Awaitility.await().until(UtilsForAPI.containerStarted(dockerClient, allContainers, email, triggerDeleteFunction));
        sa.assertTrue(allContainers.get(0).getImage().equals(triggerDeleteFunction + "-" + DataBaseManipulation.getFromDatabase(email, "user_id")));
        sa.assertAll();
    }

    @AfterClass
    public void deleteFunctions(){
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, triggerUploadFunction));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, triggerDownloadFunction));
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, triggerDeleteFunction));
    }
}