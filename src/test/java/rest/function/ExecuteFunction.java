package rest.function;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.google.gson.JsonObject;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.generators.UserBuilder;
import helpers.requests.Authentication;
import helpers.requests.BoxFile;
import helpers.requests.NavesFunction;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import helpers.utils.UtilsForAPI;
import io.restassured.RestAssured;
import org.awaitility.Awaitility;
import org.testcontainers.DockerClientFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExecuteFunction extends BaseRest {
    File file;
    String trigger;
    boolean publicAccess;
    Integer[] prID = new Integer[1];
    File executableJar;
    String link;
    String parameter = "name";
    String value = "value";
    String method = "GET";
    String contentType = "multipart/form-data";
    DockerClient dockerClient = DockerClientFactory.instance().client();
    List<Container> allContainers = new ArrayList<>();

    @BeforeClass
    public void prepareData() throws IOException {
        file = BoxFile.createRandomFile(".jar");
        executableJar = new File ("jarsForTests/renderHTML.jar");
        file.deleteOnExit();
        trigger = "GET";
        publicAccess = true;
        prID[0] = 1;
    }

    @BeforeMethod
    public void prepareForTest(){
        randomFunctionName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldExecuteFunction() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldExecuteFunctionPrivate() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, false, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotExecuteFunctionUnauthorizedToken() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, false, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token1, parameter, value, method, link, contentType).statusCode(), 403);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotExecuteFunctionInvalidToken() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction("invalidToken", parameter, value, method, link, contentType).statusCode(), 401);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotExecuteFunctionInvalidLink() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method,"invalidLink", contentType).statusCode(), 404);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }

    @Test
    public void shouldNotExecuteFunctionInvalidMethod() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language, prID).statusCode(), 201);
        Awaitility.await().until(UtilsForAPI.functionUploaded(dockerClient, allContainers, email, randomFunctionName));
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, "POST", link, contentType).statusCode(), 404);
        NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName));
        sa.assertAll();
    }
}