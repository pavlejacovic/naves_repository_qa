package rest.function;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.pojo.function.ListFunctionsResponsePojo;
import helpers.requests.BoxFile;
import helpers.requests.NavesFunction;
import helpers.utils.Property;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;

public class ListFunctions extends BaseRest {

    File file;
    String trigger;
    boolean publicAccess;
    Integer[] prID = new Integer[1];
    Response response;
    ListFunctionsResponsePojo[] responsePojo;

    @BeforeClass
    public void prepareData() throws IOException {
        file = BoxFile.createRandomFile(".jar");
        file.deleteOnExit();
        trigger = "GET";
        publicAccess = true;
        prID[0] = 1;
        randomFunctionName = GenerateRandom.randomDataName();
        for (int i = 0; i < 10; i++){
            NavesFunction.createNavesFunction(token, randomFunctionName + i, publicAccess, trigger, file, language, prID);
        }
    }

    @Test
    public void shouldListAllFunctions(){
        response = NavesFunction.listAllFunctions(token);
        responsePojo = response.getBody().as(ListFunctionsResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        for (int i = 0; i < responsePojo.length; i++){
            //sa.assertEquals(responsePojo[i].getFunctionName(), randomFunctionName + i);
            sa.assertEquals(responsePojo[i].getOwner().getEmail(), email);
            sa.assertEquals(responsePojo[i].getFunctionLink(), (Property.getProperty("noGateway") + "/lambda/api/" + DataBaseManipulation.getNavesFunctionLink(email, (randomFunctionName + i))));
            sa.assertEquals(responsePojo[i].getFunctionName(), (randomFunctionName + i));
            sa.assertEquals(responsePojo[i].getFunctionId().intValue(), DataBaseManipulation.getNavesFunctionID(email, (randomFunctionName + i)));
            sa.assertEquals(responsePojo[i].getTrigger(), trigger);
            sa.assertEquals(responsePojo[i].getPublicAccess().toString(), String.valueOf(publicAccess));
        }
    sa.assertAll();
    }

    @Test
    public void shouldListAllFunctionsWithPagination(){
        response = NavesFunction.listAllFunctionsWithPagination(token, 3, 2);
        responsePojo = response.getBody().as(ListFunctionsResponsePojo[].class);
        sa.assertEquals(response.statusCode(), 200);
        sa.assertEquals(responsePojo.length, 3);
        for (int i = 0; i < responsePojo.length; i++){
            //sa.assertEquals(responsePojo[i].getFunctionName(), randomFunctionName + i);
            sa.assertEquals(responsePojo[i].getOwner().getEmail(), email);
            sa.assertEquals(responsePojo[i].getFunctionLink(), (Property.getProperty("noGateway") + "/lambda/api/" + DataBaseManipulation.getNavesFunctionLink(email, (randomFunctionName + (6 + i)))));
            sa.assertEquals(responsePojo[i].getFunctionName(), (randomFunctionName + (6 + i)));
            sa.assertEquals(responsePojo[i].getFunctionId().intValue(), DataBaseManipulation.getNavesFunctionID(email, (randomFunctionName + (6 + i))));
            sa.assertEquals(responsePojo[i].getTrigger(), trigger);
            sa.assertEquals(responsePojo[i].getPublicAccess().toString(), String.valueOf(publicAccess));
        }
        sa.assertAll();
    }

    @Test
    public void shouldNotListAllFunctionsInvalidToken(){
        sa.assertEquals(NavesFunction.listAllFunctions("invalidTokenString").statusCode(), 401);
        sa.assertAll();
    }

    @AfterClass
    public void tearDownFunctions(){
        for (int i = 0; i < 10; i ++){
            NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName + i));
        }
    }
}