package rest.function;

import helpers.dataBase.DataBaseManipulation;
import helpers.generators.GenerateRandom;
import helpers.pojo.function.FunctionStatisticsResponsePojo;
import helpers.pojo.function.ListFunctionsResponsePojo;
import helpers.requests.Authentication;
import helpers.requests.NavesFunction;
import helpers.utils.GetUserPropertyFromJson;
import helpers.utils.Property;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import rest.BaseRest;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class FunctionStatistics extends BaseRest {
    String trigger;
    boolean publicAccess;
    Integer[] prID = new Integer[1];
    String parameter = "name";
    String value = "value";
    String method = "GET";
    String contentType = "multipart/form-data";
    File executableJar;
    String link;
    FunctionStatisticsResponsePojo[] responsePojo;

    @BeforeClass
    public void prepareData() throws IOException {

        executableJar = new File ("jarsForTests/testing.jar");
        trigger = "GET";
        publicAccess = true;
        prID[0] = 1;
        randomFunctionName = GenerateRandom.randomDataName();
    }

    @Test
    public void shouldGetStatistic() {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language,  prID).statusCode(), 201);
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        for(int i = 0; i < 3; i++) sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        Response response = NavesFunction.getStatistics(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), 10).prettyPeek();
        responsePojo = response.getBody().as(FunctionStatisticsResponsePojo[].class);
        for (FunctionStatisticsResponsePojo responses : responsePojo) {
            sa.assertEquals(responses.getNumberOfExecutions().toString(), "3");
            sa.assertEquals(responses.getDate().substring(0,9), LocalDateTime.now().toString().substring(0,9));
            sa.assertEquals(responses.getFunctionName(), randomFunctionName + "-" + DataBaseManipulation.getFromDatabase(email, "user_id"));
        }
        sa.assertEquals(NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        sa.assertAll();
    }

    //testing if statistics accurately show  executions of functions on different dates (in two json bodies in one array) (take care that daysOfExecution matches date in assert)
    @Test
    public void shouldGetStatistic365DaysAgo() throws SQLException {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language, prID).statusCode(), 201);
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        DataBaseManipulation.changeDateOfExecution(email, randomFunctionName);
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200);
        Response response = NavesFunction.getStatistics(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), 365);
        responsePojo = response.getBody().as(FunctionStatisticsResponsePojo[].class);
        for(int i = 0; i < 1; i++) {
            sa.assertEquals(responsePojo[0].getNumberOfExecutions().toString(), "1");
            sa.assertEquals(responsePojo[1].getNumberOfExecutions().toString(), "3");
            sa.assertEquals(responsePojo[0].getDate().substring(0,10), "2021-09-20");
            sa.assertEquals(responsePojo[1].getDate().substring(0,10), LocalDateTime.now().toString().substring(0,10));
            sa.assertEquals(responsePojo[0].getFunctionName(), randomFunctionName + "-" + DataBaseManipulation.getFromDatabase(email, "user_id"));
            sa.assertEquals(responsePojo[1].getFunctionName(), randomFunctionName + "-" + DataBaseManipulation.getFromDatabase(email, "user_id"));
        }
        sa.assertEquals(NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetStatisticInvalidToken()  {
        token1 = Authentication.registerLoginReturnToken(testUser1);
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language,  prID).statusCode(), 201, "create");
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200, "execution");
        sa.assertEquals(NavesFunction.getStatistics(token1, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName), 10).statusCode(), 403);
        sa.assertEquals(NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        sa.assertAll();
    }

    @Test
    public void shouldNotGetStatisticInvalidFunction()  {
        sa.assertEquals(NavesFunction.createNavesFunction(token, randomFunctionName, publicAccess, trigger, executableJar, language,  prID).statusCode(), 201, "create");
        link = (DataBaseManipulation.getNavesFunctionLink(email, randomFunctionName));
        sa.assertEquals(NavesFunction.executeFunction(token, parameter, value, method, link, contentType).statusCode(), 200, "execution");
        sa.assertEquals(NavesFunction.getStatistics(token, DataBaseManipulation.getNavesFunctionID(email, "invalidFunction"), 10).statusCode(), 404);
        sa.assertEquals(NavesFunction.deleteNavesFunction(token, DataBaseManipulation.getNavesFunctionID(email, randomFunctionName)).statusCode(), 200);
        sa.assertAll();
    }
}
