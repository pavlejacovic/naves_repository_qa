NAVES TESTING REPOSITORY

Basic instructions:

This repository consists of a set of tests and additional classes required to test the functionalities of the NAVES service. Tests were written in JAVA programming language using IntelliJ Idea IDE. At the moment, the NAVES service is not hosted on a server and it needs to be run locally before testing it, so in order to run it, you will need to have several additional applications and services installed and running, as listed below.


Starting the NAVES service:


NAVES repositories can be found on bitbucket:
Frontend: (link to frontend repository)
Backend: (link to backend repository)

Required applications and services:


To start the NAVES you will have to install and run the following applications and services:


Spring Tool Suite 4 (Version: 4.11.0.RELEASE, Build Id: 202106180608)
MySql Community edition
RabbitMQ (instructions for installation can be found here: link for instructions)
Visual Studio Code (Version 1.60.0)

Testing requirements and specifications:

To be able to run the tests first start MySql, open backend repository as Maven project in Spring Tool Suite and start NAVES services in the correct order:
1. Service-registry
2. Authentication-service
3. Project-service
4. Cloud-gateway

After that start RabbitMQ, following the instructions provided above. 
To start the frontend, in Visual Studio Code open the repository, run the command “npm install” in the terminal, and after successful installation run the command “npm start”.

Tests were conducted on two separate OS, Windows 10 and Linux (Ubuntu). IDE used is IntelliJ IDEA 2021.2 (Community Edition). Open the QA repository and as Maven project. To run all the REST tests run the “testing.xml” file. To run the UI tests in parallel on available browsers run “multiBrowserTesting”.